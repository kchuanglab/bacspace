function [segmentData, imageDataDebrisMasked]=backgroundAndThreshold(imageData,epi_mask, imageInfo, hObject)


segmentData=struct();
imageDataDebrisMasked=zeros(size(imageData), 'like', imageData);
debrisMask=false(size(imageData));
handles=guidata(hObject);
downsample=@(im) imresize(im,[NaN, handles.params.displaySize]);

downsampleFactor=size(imageData,2)/handles.params.displaySize;

for i=1:imageInfo.nChannels
    if strfind(imageInfo.bgType{i},'Opening')
        
        %remove epithelium, and also any debris removed in another
        %channel (if option is selected)
        if handles.params.crossChannelDebris
            mask=epi_mask | any(debrisMask,3);
        else
            mask=epi_mask;
        end
        
        
        
        %background-subtracted image
        
        
        imStart=imfilter(imageData(:,:,i), fspecial('gaussian', 5,1)).*(1-mask);
        
        bgSubIm=imStart;
        if handles.params.gaussianDebrisSize
            bgSubIm=bgSubIm-imfilter(bgSubIm, fspecial('gaussian', ceil(5*handles.params.gaussianDebrisSize),handles.params.gaussianDebrisSize));
        end
        if handles.params.morphoDebrisSize
            bgSubIm=bgSubIm-imopen(bgSubIm, strel('disk', handles.params.morphoDebrisSize));
        end
        
        
        bgSubIm(bgSubIm<0)=0;
        
        %debris is what we took out; only bother with stuff that hasn't
        %already been masked
        debris=(imStart-bgSubIm).*(1-mask);
        
        
        
        bgSubImDownsample=downsample(bgSubIm);
        lim=stretchlim(bgSubImDownsample,.001);
        
        imStartDownsample=imadjust(downsample(imStart),lim);
        %imStartDownsample=downsample(imStart);
        
        debrisDownsample=downsample(debris);
        scaledDebrisDilate=round(double(handles.params.debrisDilate)/downsampleFactor);
        redrawFunc=@(thresh) redrawDebris(handles.axes1,imStartDownsample, debrisDownsample,scaledDebrisDilate, thresh);
        imagesc(imStartDownsample); axis equal; axis off;
        set(handles.status, 'String','Select a threshold so that large objects are appropriately identified as debris'); pause(.00001);
        thresh=adjustmentSlider2(['Debris threshold for channel ', imageInfo.channelNames{i}], 0, 1, redrawFunc,graythresh(debris));
        segmentData.debrisThresh(i)=thresh;
        %threshold, and dilate to provide padding around debris...
        debrisMask(:,:,i)=logical(imdilate(im2bw(mat2gray(debris),thresh), strel('disk', double(handles.params.debrisDilate),0)));
        
        %BG-subbed im, with debris masked out
        maskedIm=bgSubIm.*(1-debrisMask(:,:,i));
        
        
        
        %now try to remove residual debris
        if handles.params.debrisDistance && thresh<1
            
            
            debrisDist=bwdist(debrisMask(:,:,i) | epi_mask);
            %distCut will be in units of original image, so no need to
            %scale it (or this image)
            debrisDistDownsample=downsample(debrisDist);
            redrawFunc=@(thresh) redrawExtraDebris(handles.axes1, (maskedIm),...
                debrisDist,(debrisMask(:,:,i)),...
               handles.params.debrisDilate,handles.params.debrisDistance, thresh);
           imagesc(maskedIm); axis equal; axis off;
            thresh=adjustmentSlider2(['Object threshold for extra debris removal: channel  ', imageInfo.channelNames{i}], 0, 1, redrawFunc,graythresh(maskedIm));
            [~,extraDebris]=removeExtraDebris(maskedIm, debrisDist,thresh,handles.params.debrisDistance);
            extraDebris=imdilate(extraDebris, strel('disk', double(handles.params.debrisDilate),0));
            maskedIm(extraDebris)=0;
              tmp=debrisMask(:,:,i);
        tmp(extraDebris)=1;
        debrisMask(:,:,i)=tmp;
        end
        
       
        
        
        imageDataDebrisMasked(:,:,i)=maskedIm;
        
        
        
        %otherwise,no BG subtraction
    else
        imageDataDebrisMasked(:,:,i)=(imageData(:,:,i)).*(1-epi_mask);
    end
end
 segmentData.debrisMask=debrisMask;
 
if handles.params.crossChannelDebris
    debris=any(debrisMask,3);
    
    for i=1:imageInfo.nChannels
        imageDataDebrisMasked(:,:,i)=imageDataDebrisMasked(:,:,i).*(1-debris);
    end
end


end
function []=redrawDebris(ax,redIm,debris,debrisDilate, thresh)
currAx=axis(ax);
debrisMask=logical(imdilate(im2bw(mat2gray(debris),thresh),strel('disk',debrisDilate,0)));
imagesc((redIm),'Parent', ax);

colormap(ax,'gray');
axis(ax,'equal');
axis(ax,'off');
hold(ax,'on');
debris=uint8(zeros([size(redIm),3]));
%debris(:,:,1)=uint8(256*mat2gray(debrisMask));
debris(:,:,2)=uint8(256*mat2gray(debrisMask));

h=image(debris, 'AlphaData', 0.5*debris(:,:,2), 'Parent',ax);

hold(ax,'off');
axis(ax, currAx);

end

function [imIn,extraDebris]=removeExtraDebris(imIn, debrisMaskDist, thresh, distCut)

mask=im2bw(mat2gray(imIn),thresh);
%get min distance (in debrisMaskDist transform), and identity
props=regionprops(mask, debrisMaskDist, 'minIntensity','PixelIdxList');
%remove objects smaller than this
toRemove={props([props.MinIntensity]<distCut).PixelIdxList};
imIn(vertcat(toRemove{:}))=0;
extraDebris=false(size(imIn));
extraDebris(vertcat(toRemove{:}))=true;
end


function []=redrawExtraDebris(ax, im, debrisMaskDist,debrisIm, debrisDilate, distCut, thresh)
currAx=axis(ax);
hold(ax,'off');
bitIm=uint8(zeros([size(im),3]));
bitIm(:,:,1)=uint8(256*mat2gray(imadjust(im)));
bitIm(:,:,2)=uint8(256*mat2gray(imadjust(im)));
bitIm(:,:,3)=uint8(256*mat2gray(imadjust(im)));

image(bitIm, 'Parent', ax);


colormap(ax,'gray');
axis(ax,'equal');
axis(ax,'off');
hold(ax,'on');

[clearedIm,extraDebris]=removeExtraDebris(im,debrisMaskDist,thresh,distCut);


debrisImColor=uint8(zeros([size(im),3]));
debrisImColor(:,:,1)=uint8(256*mat2gray(extraDebris));
debrisImColor(:,:,2)=uint8(256*mat2gray(debrisIm));
alphas=uint8(256*mat2gray(any(debrisImColor,3)));
hold(ax,'on');
image(debrisImColor, 'AlphaData', .5*alphas, 'Parent',ax);


hold(ax,'off');
axis(ax,currAx);
end



