function []=scatterC(contour, option)
if nargin==1
    option='b';
end
scatter(contour(:,1), contour(:,2), option);
