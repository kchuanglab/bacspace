function varargout = BacSpace(varargin)
    % BACSPACE MATLAB code for BacSpace.fig
    %      BACSPACE, by itself, creates a new BACSPACE or raises the existing
    %      singleton*.
    %
    %      H = BACSPACE returns the handle to a new BACSPACE or the handle to
    %      the existing singleton*.
    %
    %      BACSPACE('CALLBACK',hObject,eventData,handles,...) calls the local
    %      function named CALLBACK in BACSPACE.M with the given input arguments.
    %
    %      BACSPACE('Property','Value',...) creates a new BACSPACE or raises the
    %      existing singleton*.  Starting from the left, property value pairs are
    %      applied to the GUI before BacSpace_OpeningFcn gets called.  An
    %      unrecognized property name or invalid value makes property application
    %      stop.  All inputs are passed to BacSpace_OpeningFcn via varargin.
    %
    %      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
    %      instance to run (singleton)".
    %
    % See also: GUIDE, GUIDATA, GUIHANDLES
    
    % Edit the above text to modify the response to help BacSpace
    
    % Last Modified by GUIDE v2.5 25-Sep-2014 10:20:15
    % 
    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
        'gui_Singleton',  gui_Singleton, ...
        'gui_OpeningFcn', @BacSpace_OpeningFcn, ...
        'gui_OutputFcn',  @BacSpace_OutputFcn, ...
        'gui_LayoutFcn',  [] , ...
        'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end
    
    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
    % End initialization code - DO NOT EDIT
end

% --- Executes just before BacSpace is made visible.
function BacSpace_OpeningFcn(hObject, eventdata, handles, varargin)
    % This function has no output args, see OutputFcn.
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    % varargin   command line arguments to BacSpace (see VARARGIN)
    
    % Choose default command line output for BacSpace
    handles.output = hObject;
    
    
    
    
    handles.reload=0;
    handles.buttonDown=0;
    handles.busy=0;
    
    handles.zPos=1;
    
    global interrupt;
    interrupt=0;
    
    
    if numel(dir('currDir.mat'))
        tmp=load('currDir.mat');
        if ischar(tmp.dirname) && isdir(tmp.dirname)
        handles.currDir=tmp.dirname;
        else
             handles.currDir='.';
        end
    else
        
        handles.currDir='.';
    end
    handles.params=initParamsGut();
    paramsFileName='initParamsGut.m';
    
    handles.comments=importComments(paramsFileName);
    
    % UIWAIT makes contours wait for user response (see UIRESUME)
    % uiwait(handles.figure1);
    set(handles.figure1, 'WindowButtonDown', @buttonDown);
    set(handles.figure1, 'WindowButtonUp', @buttonUp);
    
    set(handles.figure1, 'WindowButtonMotionFcn', @motion);
    
    
    
    
    guidata(hObject, handles);
    imagesc(rand(100), 'Parent', handles.axes1); axis equal; axis off;
    
    
    
    % Update handles structure
    
end

function figure1_KeyPressFcn(varargin)
end

function figure1_ButtonDownFcn(varargin)
end
function figure1_WindowKeyPressFcn(varargin)
end


% UIWAIT makes BacSpace wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = BacSpace_OutputFcn(hObject, eventdata, handles)
    % varargout  cell array for returning output args (see VARARGOUT);
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % Get default command line output from handles structure
    varargout{1} = handles.output;
end


function buttonUp(source, event)
    handles=guidata(source);
    handles.buttonDown=0;
    guidata(source,handles);
end

function buttonDown(source, event)
    handles=guidata(source);
    handles.buttonDown=1;
    guidata(source,handles);
end

function motion(source, event)
end






% --- Executes on button press in drawContour.
function drawContour_Callback(hObject, eventdata, handles)
    %if isfield(handles, 'epiData') & handles.zPos>1 & numel(handles.epiData)>=(handles.zPos-1) & isfield(handles.epiData(handles.zPos-1), 'C2')
    if handles.zPos>1 & numel(handles.epiData(handles.zPos-1).C2)
        guess=handles.epiData(handles.zPos-1).C2;
    else
        guess=[];
    end
    
    if ~isfield(handles,'imageInfo')
        warn('No image loaded');
        return;
    end
    
    epiData=getEpithelium(hObject,guess);
    handles=guidata(hObject);
    if isstruct(epiData)
        try
            handles.epiData(handles.zPos)=epiData;
        catch
            warn('Failed to get contour');
        end
    end
    
    guidata(hObject, handles);
    
    
    
end
% hObject    handle to drawContour (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% hObject    handle to drawContour (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in load.
function load_Callback(hObject, eventdata, handles)
    
    % hObject    handle to load (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    handles=guidata(hObject);
    if isfield(handles, 'stitchedImage')
        handles=rmfield(handles, 'stitchedImage');
    end
    guidata(hObject,handles);
    loadFile(hObject);
end

function loadFile(hObject)
    handles=guidata(hObject);
    if isfield(handles, 'stitchedImage')
        %disp('stiched image found');
        handles.stitchedImage
        handles.imageInfo=loadBF('',handles.stitchedImage);
    else
        handles.imageInfo=loadBF(handles.currDir);
        if (~isstruct(handles.imageInfo))
            warn('No file loaded');
            return
        end
    end
    handles.epiData(handles.imageInfo.nZPlanes)=struct('C2', []);
    handles.currDir=handles.imageInfo.pathname;
    guidata(hObject,handles);
    %cla(handles.axes1);
    redraw(hObject,handles);
    handles=guidata(hObject);
    
    set(handles.status, 'String','File loaded');
    
    guidata(hObject,handles);
    
    
end


% --- Executes on button press in saveContour.
function saveContour_Callback(hObject, eventdata, handles)
    
    handles=guidata(hObject);
    if ~isfield(handles, 'epiData') || ~isstruct(handles.epiData) ||  ~numel(handles.epiData) || (~isstruct(handles.epiData) && isnan(handles.epiData))
        warn('Nothing to save')
        return;
    end
    for i=1:numel(handles.epiData)
        if numel(handles.epiData(i).C2)
            
            epiData(i)=addEdges(hObject,i);
        end
    end
    if isfield(handles, 'currentImage')
        handles=rmfield(handles, 'currentImage');
    end
    handles.epiData=epiData;
    dotInd=find(handles.imageInfo.filename=='.', 1);
    outname=[handles.imageInfo.filename(1:dotInd-1),'_contour',datestr(now,'yymmdd'),'.mat'];
    handles.contourDataFileName=outname;
    handles.contourDataFileDir=handles.imageInfo.pathname;
           guidata(hObject, handles);

    handles.imageInfo.reader=[];
    handles=removeFigureHandles(handles);
    save(fullfile(handles.imageInfo.pathname, outname), 'handles');
    handles=guidata(hObject);
    set(handles.dataFileNameText, 'String', outname);
    handles=rmfield(handles, 'epiData');
    
    guidata(hObject, handles);
    set(handles.status, 'String','Saved data');
    
    
end
% hObject    handle to saveContour (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in segment.
function segment_Callback(hObject, eventdata, handles)
    % hObject    handle to segment (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % m=msgbox('In the next window please select a .mat file with a user-drawn contour, such as you drew in the previous step','modal')
    % uiwait(m);
    if numel(handles.contourDataFileName)
        
        
        h=load(fullfile(handles.contourDataFileDir,handles.contourDataFileName));
        handles.imageInfo=h.handles.imageInfo;
        handles.imageInfo.reader=bfGetReader(fullfile(handles.imageInfo.pathname, handles.imageInfo.filename));
        handles.epiData=h.handles.epiData;
        numZSlices=0;
        for z=1:numel(handles.epiData)
            
            if numel(handles.epiData(z).C2)
                numZSlices=numZSlices+1;
            end
        end
        if isfield(handles,'segmentData')
            rmfield(handles,'segmentData');
        end
        count=0;
        for z=1:numel(handles.epiData)
            if numel(handles.epiData(z).C2)
                
                handles.epiData(z)=fixOrientation(handles.epiData(z), [handles.imageInfo.numRows, handles.imageInfo.numCols]);
                
                
                for i=1:handles.imageInfo.nChannels
                    imageData(:,:,i)=mat2gray(getImage(handles.imageInfo, z, i));
                end
                set(handles.status, 'String',['Read in z-slice ', num2str(z), ' of ', num2str(numZSlices)]); pause(.01);
                %shift so that contour points are all but last 3;
                count=count+1;
                labelStr=['Z-slice ', num2str(count), ' of ', num2str(numZSlices)];
                
                
                
                [numY,numX]=size(imageData(:,:,handles.imageInfo.boundaryChannelNumber));
                
                
                
                colormap('gray');
                imagesc(imageData(:,:,handles.imageInfo.boundaryChannelNumber)); axis off
                pause(.01);
                set(handles.status, 'String',['Computing epi mask (could be slow): ',labelStr]); pause(.01);
                
                epi_mask=logical(computeMask(handles.epiData(z).C_closed,numX,numY,handles.params.maskScaleFactor));
                
                
                
                
                epi_mask=logical(imdilate(epi_mask, strel('disk', double(handles.params.debrisDilate),0)));
                
                set(handles.status, 'String', ['Computed epithelial mask: ',labelStr]); pause(.01);
                
                
                %disp('thresholding');
                [segmentData, imageDataDebrisMasked]=backgroundAndThreshold(imageData, epi_mask,handles.imageInfo, hObject);
                
                
                
                
                
                
                
                
                set(handles.status, 'String',['Done with thresholding: ', labelStr]); pause(.01);
                
                set(handles.status, 'String',['Computing distance to contours: ', labelStr]); pause(.01);
                pause(.01);
                
                ds=distanceToContour(handles.epiData(z).C_open, numX,numY, handles.params.useFastDistance);
                set(handles.status, 'String',['Computed distance to contours: ', labelStr]); pause(.01);
                
                
                %if handles.params.crossChannelDebris
                
                %   debrisMask=imageDataDebrisMasked(:,:,handles.imageInfo.boundaryChannelNumber)==0;
                %if handles.params.debrisDilate
                %   debrisMask=logical(imdilate(debrisMask, strel('disk', double(handles.params.debrisDilate),0)));
                %end
                
                %for i=1:size(imageDataDebrisMasked,3)
                %   imageDataDebrisMasked(:,:,i)=imageDataDebrisMasked(:,:,i).*(1-debrisMask);
                %end
                %clear('debrisMask');
                %end
                
                
                segmentData.imageData=imageData;
                clear('imageData');
                segmentData.epi_mask=epi_mask;
                %disp('epi_mask');
                segmentData.imageDataDebrisMasked=imageDataDebrisMasked;
                clear('imageDataDebrisMasked');
                %disp('ImageDataDebrisMasked');
                segmentData.epiImage=segmentData.imageData(:,:,handles.imageInfo.boundaryChannelNumber).*epi_mask;
                %disp('epi_cells');
                segmentData.bacteriaImage=segmentData.imageDataDebrisMasked(:,:,handles.imageInfo.boundaryChannelNumber);
                %disp('bacteria');
                segmentData.debrisImage=(segmentData.imageData(:,:,handles.imageInfo.boundaryChannelNumber)-segmentData.imageDataDebrisMasked(:,:,handles.imageInfo.boundaryChannelNumber)).*(1-epi_mask);;
                %disp('debris');
                segmentData.bacteriaWithDebrisImage=segmentData.imageDataDebrisMasked(:,:,handles.imageInfo.boundaryChannelNumber).*(1-epi_mask);
                %disp('bact with debris');
                
                %disp('imagedata');
                segmentData.ds_mat=reshape(ds,size(segmentData.bacteriaImage));
                %disp('ds');
                
                handles.segmentData(z)=segmentData;
                clear('segmentData');
                %disp('segmentdata');
                mucInd=find(cellfun(@numel, strfind(lower(handles.imageInfo.channelNames),'muc')),1);
                
                if numel(mucInd)==0
                    channelNums=1:handles.imageInfo.nChannels;
                    channelNums(channelNums==handles.imageInfo.boundaryChannelNumber)=[];
                    if numel(channelNums)
                        mucInd=channelNums(1);
                    else
                        mucInd=1;
                    end
                end
                if handles.params.drawMerge
                    redrawMerge(handles,imadjust(handles.segmentData(z).bacteriaImage), handles.segmentData(z).imageDataDebrisMasked(:,:,mucInd).*(1-epi_mask),...
                        handles.segmentData(z).epiImage,handles.segmentData(z).debrisImage, handles.epiData(z).C_open);
                end
                %disp('redraw');
                basename=[datestr(now,'yymmdd'),'_',handles.contourDataFileName];
                basename=strrep(basename, '.mat','');
                basedir=handles.contourDataFileDir;
                set(handles.status, 'String', ['Writing to file: ',labelStr]); pause(.01);
                handles.currDir=fullfile(basedir, basename);
                handles.basename=basename;
                mkdir(fullfile(basedir,basename));
                imwrite(to16Bit(handles.segmentData(z).epiImage), fullfile(basedir,basename, ['epi_cells', num2str(z),'.tif']));
                rmfield(handles.segmentData(z),'epiImage');
                imwrite(to16Bit(handles.segmentData(z).bacteriaImage), fullfile(basedir,basename,[handles.imageInfo.channelNames{handles.imageInfo.boundaryChannelNumber},num2str(z),'.tif']));
                rmfield(handles.segmentData(z),'bacteriaImage');
                imwrite(to16Bit(handles.segmentData(z).debrisImage), fullfile(basedir,basename, ['debris', num2str(z),'.tif']));
                rmfield(handles.segmentData(z),'debrisImage');
               % imwrite(to16Bit(handles.segmentData(z).bacteriaWithDebrisImage), fullfile(basedir,basename, [handles.imageInfo.channelNames{handles.imageInfo.boundaryChannelNumber},'_plus_debris', num2str(z),'.tif']));
                rmfield(handles.segmentData(z),'bacteriaWithDebrisImage');
                
                imwrite(logical(handles.segmentData(z).epi_mask),fullfile(basedir,basename, ['epiMask', num2str(z),'.tif']));

                for i=1:handles.imageInfo.nChannels
                    if i~=handles.imageInfo.boundaryChannelNumber
                        %imwrite(to16Bit(handles.segmentData(z).imageData(:,:,i)),fullfile(basedir,basename, [handles.imageInfo.channelNames{i},'_raw', num2str(z),'.tif']));
                        %imwrite(to16Bit(handles.segmentData(z).imageDataDebrisMasked(:,:,i)), fullfile(basedir,basename, [handles.imageInfo.channelNames{i}, num2str(z),'.tif']));
                        imwrite(to16Bit(handles.segmentData(z).imageDataDebrisMasked(:,:,i).*(1-epi_mask)),fullfile(basedir,basename, [handles.imageInfo.channelNames{i}, num2str(z),'.tif']));
                        imwrite(logical(handles.segmentData(z).debrisMask(:,:,i)),fullfile(basedir,basename, [handles.imageInfo.channelNames{i},'_debrisMask', num2str(z),'.tif']));

                    end
                    imwrite(logical(handles.segmentData(z).debrisMask(:,:,handles.imageInfo.boundaryChannelNumber)),fullfile(basedir,basename, [handles.imageInfo.channelNames{handles.imageInfo.boundaryChannelNumber},'_debrisMask', num2str(z),'.tif']));

                    
                end
                rmfield(handles.segmentData(z),'imageData');
                rmfield(handles.segmentData(z),'imageDataDebrisMasked');
                
                
                if handles.params.drawMerge
                    f=figure();
                    redrawMerge(handles, imadjust(handles.segmentData(z).bacteriaImage), handles.segmentData(z).imageDataDebrisMasked(:,:,mucInd).*(1-epi_mask),handles.segmentData(z).epiImage, handles.segmentData(z).debrisImage, handles.epiData(z).C_open);
                    axis equal; axis tight; axis off;
                    print(gcf, fullfile(basedir,handles.basename,['merge', num2str(z),'.eps']), '-depsc' ,'-r300');
                    close(f);
                end
            end
        end
        handles.imageInfo.reader=[];
        guidata(hObject,handles);
        handles=removeFigureHandles(handles);
        save(fullfile(basedir,basename, 'segmented.mat'), 'handles', '-v7.3');
        handles=guidata(hObject);
        
        set(handles.status, 'String', 'Done with segmenting'); pause(.01);
        
        handles.segmentDataPathame=fullfile(basedir,basename);
        handles.segmentDataFilename='segmented.mat';
        
        guidata(hObject, handles);
    else
        set(handles.status, 'String', 'No data set selected'); pause(.01);
    end
    
    
    
end




function redrawMerge(handles, redIm, greenIm, blueIm, yellowIm,contour)
    
    
    
    merge=uint8(zeros([size(redIm),3]));
    merge(:,:,1)=uint8(256*(mat2gray(redIm)));
    merge(:,:,2)=uint8(256*(mat2gray(greenIm)));
    merge(:,:,3)=uint8(256*(mat2gray(blueIm)));
    
    debris=uint8(zeros(size(merge)));
    debris(:,:,1)=uint8(256*mat2gray(yellowIm));
    debris(:,:,2)=uint8(256*mat2gray(yellowIm));
    
    
    %         if yellowVal+redVal>1
    %             yellowVal=1-redVal;
    %         end
    %         if yellowVal+greenVal>1
    %             yellowVal=1-greenVal;
    %         end
    %set(handles.sliderYellow, 'Value',yellowVal);
    
    image(merge);
    hold on;
    h=image(debris);
    set(h, 'AlphaData',debris(:,:,1));
    hold on
    plot(contour(:,1), contour(:,2),'c', 'LineWidth', 2);
    hold off;
    
end




% --- Executes on button press in straighten.
function straighten_Callback(hObject, eventdata, handles)
    % hObject    handle to straighten (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    m=msgbox('Now please select a segmented data set');
    uiwait(m);
    handles=guidata(hObject);
    
    [matfile,dirname]=uigetfile(fullfile(handles.currDir,'*segmented.mat'));
    if ~numel(matfile) || all(matfile==0)
        return
    end
    handles.currDir=dirname;
    guidata(hObject,handles);
    set(handles.status, 'String', 'Loading data'); pause(.01);
    pause(.01);
    h=load(fullfile(dirname,matfile));
    handles.imageInfo=h.handles.imageInfo;
    handles.imageInfo.reader=[];%bfGetReader(fullfile(handles.imageInfo.pathname, handles.imageInfo.filename));
    handles.epiData=h.handles.epiData;
    handles.segmentData=h.handles.segmentData;
   
    for z=1:numel(handles.segmentData)
        
    ds_mat(:,:,z)=handles.segmentData(z).ds_mat;
    epi_mask(:,:,z)=handles.segmentData(z).epi_mask;
    debrisMask(:,:,z)=any(handles.segmentData(z).debrisMask,3);
    end
    
    
    numZSlices=0;
    
    
    
    nPoints=inputdlg({'Enter max distance from contour to measure (in pixels)'});
    nPoints=str2num(nPoints{1});
    for z=1:numel(handles.epiData)
        
        if numel(handles.epiData(z).C2)
            numZSlices=numZSlices+1;
        end
    end
    count=0;
    
    downsampleIm=@(im) imresize(im, 1.0/handles.params.straightenDownsampleImage);
    upsampleIm=@(im) imresize(im,handles.params.params.straightenDownsampleImage);
    for z=1:numel(handles.epiData)
        
        if numel(handles.epiData(z).C2)
            count=count+1;
            labelStr=['Z-slice ', num2str(count), ' of ', num2str(numZSlices)];
            
            
            
            
            
            
            
            
            for i=1:handles.imageInfo.nChannels
%                 if i~=handles.imageInfo.boundaryChannelNumber
                    maskedImageData(:,:,i)=downsampleIm(imread(fullfile(dirname, [handles.imageInfo.channelNames{i},num2str(z),'.tif'])));
%                 else
%                     
%                     bdryIm=imread(fullfile(dirname,[handles.imageInfo.channelNames{handles.imageInfo.boundaryChannelNumber},num2str(z),'.tif']));
%                     tmpMask= downsampleIm(bdryIm==0);
%                     
%                     bacIm=downsampleIm(bdryIm);
%                     bacIm(bacIm==0)=NaN;
%                     bacIm(tmpMask)=NaN;
%                     maskedImageData(:,:,i)=bacIm;
%                 end
            end
            
            
            ds_mat=downsampleIm(ds_mat(:,:,z));
            ds_mat(downsampleIm(epi_mask(:,:,z)))=NaN;
            ds_mat(downsampleIm(debrisMask(:,:,z)))=NaN;
            %ds_mat(isnan(bacIm))=NaN;
            ds_mat=ds_mat/handles.params.straightenDownsampleImage;
            clear('bacIm');
            
            C_open=handles.epiData(z).C_open;
            L=round(sum(sqrt(sum((C_open(1:end-1,:)-C_open(2:end,:)).^2,2))));
            Csm=gaussSmoothCOpen(C_open, handles.params.straightenContourSmooth);
            C_smooth=interparc(round(L/handles.params.straightenDownsampleContour), Csm(:,1), Csm(:,2), 'linear');
            
            C_smooth=rescaleContour(C_smooth, size(handles.segmentData(z).ds_mat), size(ds_mat));
            
            nPointsDownsample=round(nPoints/handles.params.straightenDownsampleImage);
            set(handles.status, 'String', ['Straightening epithelium: ', labelStr]); pause(.01);
            updateFunc=@(s) set(handles.status,'String', [labelStr, ': ',s]);
            
            maskedImageData(:,:,end+1)=ones(size(maskedImageData(:,:,1)));
            [profileData.straightenedIm,dsStraight]=straightenImageFromContour(C_smooth, maskedImageData,ds_mat,0, nPointsDownsample, handles.params.straightenKernelWidth,handles.params.writeVoronoi,updateFunc);
            
            
            
            rescaleProfile=@(prof) imresize(prof, [L, nPoints], 'bilinear');
            
            straightenedIm=zeros([L,nPoints,size(profileData.straightenedIm,3)]);
            %mask=rescaleProfile(any(profileData.straightenedIm==0,3));
            for i=1:size(profileData.straightenedIm,3)
                straightenedIm(:,:,i)=rescaleProfile(profileData.straightenedIm(:,:,i));%.*(1-mask);
            end
            
            profileData.straightenedIm=straightenedIm(:,:,1:end-1);
            profileData.controlIm=straightenedIm(:,:,end);
            
            
            if handles.params.drawMerge
                merge=zeros([size(profileData.straightenedIm,1), size(profileData.straightenedIm,2), 3]);
                merge(:,:,1)=mat2gray(profileData.straightenedIm(:,:,1));
                mucInd=find(cellfun(@numel, strfind(lower(handles.imageInfo.channelNames),'muc')),1);
                if numel(mucInd)==0
                     channelNums=1:handles.imageInfo.nChannels;
                    channelNums(channelNums==handles.imageInfo.boundaryChannelNumber)=[];
                    if numel(channelNums)
                        mucInd=channelNums(1);
                    else
                        mucInd=1;
                    end
                end
                if handles.imageInfo.nChannels>1
                    merge(:,:,2)=mat2gray(profileData.straightenedIm(:,:,mucInd));
                end
                f=figure;
                imagesc(merge); axis off
                print(gcf, fullfile(dirname,['merge_straightened', num2str(z),'.eps']), '-depsc' ,'-r300');
                profileData.merge=merge;
                close(f);
                
            end
            
            
            
            handles.profileData(z)=profileData;
            
        end
    end
    if handles.params.drawMerge
        imagesc(merge); axis off
    end
    set(handles.status, 'String', 'Writing to file'); pause(.01);
    for z=1:numel(handles.profileData)
        if numel(handles.profileData(z).straightenedIm)
            for i=1:handles.imageInfo.nChannels
                imwrite(to16Bit(handles.profileData(z).straightenedIm(:,:,i)), fullfile(dirname, ['straightened','_z',num2str(z),'_c',num2str(i),'.tif']));
            end
            imwrite(to16Bit(handles.profileData(z).controlIm), fullfile(dirname, ['straightened','_z',num2str(z),'_ctl.tif']));
            
        end
        
    end
    
    handles.segmentData=[];
    handles.epiData=[];
    handles.imageInfo.reader=[];

    guidata(hObject,handles);
    handles=removeFigureHandles(handles);
    save(fullfile(dirname,'profiles.mat'),'handles');
    if numel(strfind(lastwarn,'7.3'))
         save(fullfile(dirname,'profiles.mat'),'handles','-v7.3');
    end
    handles=guidata(hObject);
    set(handles.status, 'String', 'Done!'); pause(.01);
    guidata(hObject, handles);
    
end




% --- Executes on button press in densityVsDistance.
function densityVsDistance_Callback(hObject, eventdata, handles)
    % hObject    handle to densityVsDistance (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    handles=guidata(hObject);
    [matfile,dirname]=uigetfile(fullfile(handles.currDir,'profiles.mat'));
    if ~numel(matfile) || all(matfile==0)
        return
    end
    handles.currrDir=dirname;
    guidata(hObject,handles);
    h=load(fullfile(dirname,matfile));
    handles.profileData=h.handles.profileData;
    handles.imageInfo=h.handles.imageInfo;
    
    
    colors=['r', 'g', 'b', 'c', 'y', 'm'];
    colorNames={'Red', 'Green', 'Blue', 'Cyan', 'Yellow', 'Magenta'};
    mucInd=find(cellfun(@numel, strfind(lower(handles.imageInfo.channelNames),'muc')),1);
    if numel(mucInd)>0 & mucInd~=2
        tmp=colors(mucInd);
        colors(mucInd)=colors(2);
        colors(2)=tmp;
        
        tmp=colorNames{mucInd};
        colorNames{mucInd}=colorNames{2};
        colorNames{2}=tmp;
    end
    
    
    
    f=figure;
    numZSlices=0;
    for z=1:numel(handles.profileData)
        
        if numel(handles.profileData(z).straightenedIm)
            numZSlices=numZSlices+1;
        end
    end
    count=0;
    
    for z=1:numel(handles.profileData)
        if numel(handles.profileData(z).straightenedIm)
            count=count+1;
            labelStr=['Z-slice ', num2str(count), ' of ', num2str(numZSlices)];
            
            
            nrows=size(handles.profileData(z).straightenedIm,1);
            ncols=size(handles.profileData(z).straightenedIm,2);
            
            if abs(handles.imageInfo.pixelSizeX-handles.imageInfo.pixelSizeY)<10^-5
                scale=handles.imageInfo.pixelSizeX;
            else
                warning('Warning: non-isotropic pixel size; no scale info included');
                scale=1;
            end
            
            for i=1:size(handles.profileData(z).straightenedIm,3)
                profNAN=handles.profileData(z).straightenedIm(:,:,i);
                profNAN(handles.profileData(z).controlIm==0)=NaN;
                dd.profilesRaw{i}=profNAN;
                dd.profilesZscore{i}=nanzscore(nanmean(profNAN));
                dd.xs=scale*(1:numel(dd.profilesZscore{i}));
                colors;
                i;
                colors(i);
                plot(dd.xs,dd.profilesZscore{i},colors(i));
                titleText{i}=[handles.imageInfo.channelNames{i}, ': ', colorNames{i}];
                
                hold on
            end
            
            
            xlabel('Distance from epithelium (microns)');
            ylabel('Relative density');
            title(titleText);
            fig2pretty(fullfile(dirname,['mean_relative_density_vs_distance',num2str(z),'.eps']));
            hold off
            densityDistData(z)=dd;
            
        end
    end
    set(handles.status, 'String', 'Done');
    guidata(hObject, handles);
    
    save(fullfile(dirname,'densityDistData.mat'), 'densityDistData');
    if numel(strfind(lastwarn,'7.3'))
         save(fullfile(dirname,'densityDistData.mat'),'handles','-v7.3');
    end
end

% --- Executes on button press in bug_report.
function bug_report_Callback(hObject, eventdata, handles)
    % hObject    handle to bug_report (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    msg=inputdlg('Please describe the bug and what you were doing when it occured. Also copy any error messages if possible', 'Bug report',10);
    msg=msg{1};
    for i=1:size(msg,1)
        msgCell{i}=msg(i,:);
    end
    
    myaddress = 'gBillingsBugReport@gmail.com';
    mypassword = 'insecure...butwhocares';
    
    
    
    setpref('Internet','E_mail',myaddress);
    setpref('Internet','SMTP_Server','smtp.gmail.com');
    setpref('Internet','SMTP_Username',myaddress);
    setpref('Internet','SMTP_Password',mypassword);
    
    props = java.lang.System.getProperties;
    props.setProperty('mail.smtp.auth','true');
    props.setProperty('mail.smtp.socketFactory.class', ...
        'javax.net.ssl.SSLSocketFactory');
    props.setProperty('mail.smtp.socketFactory.port','465');
    
    sendmail('gbillings@gmail.com', 'Bug report', msgCell);
    m=msgbox('Thank you for your feedback. Your report has been sent to Gabe!','modal');
    uiwait(m);
end


% --- Executes on button press in selectDataFile.
function selectDataFile_Callback(hObject, eventdata, handles)
    % hObject    handle to selectDataFile (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    handles=guidata(hObject);
    [epi_file,epi_dir]=uigetfile(fullfile(handles.currDir,'*.mat'),'Select contour file');
    handles.currDir=epi_dir;
    guidata(hObject,handles);
    handles.numChannels=1;
    handles.contourDataFileName=epi_file;
    handles.contourDataFileDir=epi_dir;
    handles.channelNames{1}='boundary';
    
    set(handles.dataFileNameText, 'String', epi_file);
    
    guidata(hObject, handles);
end

% --- Executes on button press in plotBoundaryPositions.
function plotBoundaryPositions_Callback(hObject, eventdata, handles)
    % hObject    handle to plotBoundaryPositions (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    handles=guidata(hObject);
    [matfile,dirname]=uigetfile(fullfile(handles.currDir,'profiles.mat'));
    if ~numel(matfile) || all(matfile==0)
        return
    end
    handles.currDir=dirname;
    guidata(hObject,handles);
    
    h=load(fullfile(dirname,matfile));
    handles.imageInfo=h.handles.imageInfo;
    handles.profileData=h.handles.profileData;
    colors=['r', 'g', 'b', 'c', 'y', 'm'];
    colorNames={'Red', 'Green', 'Blue', 'Cyan', 'Yellow', 'Magenta'};
    mucInd=find(cellfun(@numel, strfind(lower(handles.imageInfo.channelNames),'muc')),1);
    if numel(mucInd)>0 & mucInd~=2
        tmp=colors(mucInd);
        colors(mucInd)=colors(2);
        colors(2)=tmp;
        
        tmp=colorNames{mucInd};
        colorNames{mucInd}=colorNames{2};
        colorNames{2}=tmp;
    end
    
    numZSlices=0;
    for z=1:numel(handles.profileData)
        
        if numel(handles.profileData(z).straightenedIm)
            numZSlices=numZSlices+1;
        end
    end
    count=0;
    
    for z=1:numel(handles.profileData)
        if numel(handles.profileData(z).straightenedIm)
            count=count+1;
            labelStr=['Z-slice ', num2str(count), ' of ', num2str(numZSlices)];
            
            
            if abs(handles.imageInfo.pixelSizeX-handles.imageInfo.pixelSizeY)<10^-5
                scale=handles.imageInfo.pixelSizeX;
            else
                warn('Warning: non-isotropic pixel size; no scale info included');
                scale=1;
            end
            
            
            statusFunc=@(x) set(handles.status,'String',x);
            
            interpolatedPoints=handles.profileData(z).controlIm<0.999;
            interpolatedPoints(:,1:handles.params.debrisDilate)=0;
            interpolatedPoints=imdilate(interpolatedPoints,strel('disk',2*handles.params.straightenDownsampleContour,0));
            for i=1:size(handles.profileData(z).straightenedIm,3)
                m=msgbox(['Now select a few boundary points for channel ',handles.imageInfo.channelNames{i},'; or right-click to skip channel. Press enter when done in both cases'] );
                uiwait(m);
                [edgeLocs{i}, interpolated{i}]=edgeFromProfile(handles.profileData(z).straightenedIm(:,:,i),interpolatedPoints, statusFunc,handles.params.mucusEdgeZeroThresh);
                edgeLocs{i}=scale*edgeLocs{i};
            end
            f=figure;
            
            for i=1:size(handles.profileData(z).straightenedIm,3)
                xs=1:numel(edgeLocs{i});
                ys=edgeLocs{i};
                ysInterp=ys;
                interp=logical(interpolated{i});
                ys(interp)=NaN;
                ysInterp(~interp)=NaN;
                plot(xs,ys, colors(i));
                hold on
                plot(xs,ysInterp,[colors(i),':']);
                titleText{i}=[handles.imageInfo.channelNames{i}, ': ', colorNames{i}];
            end
            
            bd.edgeLocs=edgeLocs;
            bd.interpolated=interpolated;
            
            xlabel('Distance along epithelium(microns)');
            ylabel('Distance from epithelium (microns)');
            title(titleText);
            fig2pretty(fullfile(dirname, ['boundary_positions', num2str(z), '.eps']));
            hold off
            
            for i=1:size(handles.profileData(z).straightenedIm,3)
                for j=1:(i-1)
                    x=edgeLocs{i};
                    y=edgeLocs{j};
                    
                    scatter(x(interpolated{i}==0),y(interpolated{i}==0));
                    xlabel([handles.imageInfo.channelNames{i}, ' edge location (microns)']);
                    ylabel([handles.imageInfo.channelNames{j}, ' edge location (microns)']);
                    fig2pretty(fullfile(dirname, ['edge_',handles.imageInfo.channelNames{i}, '_vs_', handles.imageInfo.channelNames{j},num2str(z), '.eps']));
                    
                end
            end
            boundaryData(z)=bd;
            close(f);
        end
    end
    
    save(fullfile(dirname,'boundaryData.mat'), 'boundaryData')
    set(handles.status, 'String', 'Done');
    guidata(hObject, handles);
end





% --- Executes on button press in zDown.
function zDown_Callback(hObject, eventdata, handles)
    % hObject    handle to zDown (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    
    handles=guidata(hObject);
    
    
    if isfield(handles, 'imageInfo') && numel(handles.imageInfo)
        if handles.zPos>1
            handles.zPos=handles.zPos-1;
        end
        guidata(hObject,handles);
        redraw(hObject,handles);
        handles=guidata(hObject);
        set(handles.zPosText, 'String', ['z=',num2str(handles.zPos)]);
        guidata(hObject, handles);
    end
end

% --- Executes on button press in zUp.
function zUp_Callback(hObject, eventdata, handles)
    % hObject    handle to zUp (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    if isfield(handles, 'imageInfo') && numel(handles.imageInfo)
        
        handles=guidata(hObject);
        
        if handles.zPos<handles.imageInfo.nZPlanes
            handles.zPos=handles.zPos+1;
        end
        guidata(hObject,handles);
        
        redraw(hObject,handles);
        handles=guidata(hObject);
        set(handles.zPosText, 'String', ['z=',num2str(handles.zPos)]);
        
        guidata(hObject, handles);
    end
    
    
end
function redraw(hObject,handles)
    handles=guidata(hObject);
    set(handles.status, 'String', 'Loading image');
    pause(.0001);
    handles.currentImage=getImage(handles.imageInfo, handles.zPos,handles.imageInfo.boundaryChannelNumber);
    guidata(hObject,handles);
    colormap(gray)
    
    imagesc(handles.currentImage, 'Parent',handles.axes1);
    
    hold on
    try
        plotC(4*handles.epiData(handles.zPos).C2, 'r');
    end
    hold off
    axis([0,handles.imageInfo.numCols, 0, handles.imageInfo.numRows])
    axis equal;
    axis tight;
    guidata(hObject,handles);
    
end



% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over drawContour.
function drawContour_ButtonDownFcn(hObject, eventdata, handles)
    % hObject    handle to drawContour (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
end


% --- Executes on button press in fixButton.
function fixButton_Callback(hObject, eventdata, handles)
    % hObject    handle to fixButton (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
end






% --- Executes on button press in stitch.
function stitch_Callback(hObject, eventdata, handles)
    % hObject    handle to stitch (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    handles=guidata(hObject);
    [outName,status]=stitchImagesGUI(handles.status);
    if numel(outName) && status
        handles.stitchedImage=outName;
        
        guidata(hObject,handles);
        loadFile(hObject);
    end
end
% --- Executes on button press in params.
function params_Callback(hObject, eventdata, handles)
    
    newParams=setParams(handles.params,handles.comments);
    handles=guidata(hObject);
    handles.params=newParams;
    guidata(hObject,handles);
    
    
end

% hObject    handle to params (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in loadParams.
function loadParams_Callback(hObject, eventdata, handles)
    % hObject    handle to loadParams (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    handles=guidata(hObject);
    [filename, dirname]=uigetfile(fullfile(handles.currDir, '*.mat'),'Select file containing parameters');
    handles.currDir=dirname;
    guidata(hObject,handles);
    set(handles.status,'String','Loading parameters');
    drawnow 
    s=load(fullfile(dirname,filename));
    set(handles.status,'String','Read parameter file');
    drawnow
    handles=guidata(hObject);
    if isfield(s,'handles') && isfield(s.handles, 'params')
        params=s.handles.params;
        if isequal(fieldnames(orderfields(handles.params)), fieldnames(orderfields(params)))
            handles.params=params;
            guidata(hObject,handles);
        else
            warn('Invalid parameters; probably from older version; no parameters loaded');
        end
        
    else
        warn( 'No parameters found in file specified no parameters loaded');
    end
        set(handles.status,'String','Done loading parameters');
        drawnow
end


% --- Executes on button press in densityVsPosition.
function densityVsPosition_Callback(hObject, eventdata, handles)
    % hObject    handle to densityVsPosition (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    handles=guidata(hObject);
    [matfile,dirname]=uigetfile(fullfile(handles.currDir,'profiles.mat'));
    if ~numel(matfile) || all(matfile==0)
        return
    end
    handles.currrDir=dirname;
    guidata(hObject,handles);
    h=load(fullfile(dirname,matfile));
    handles.profileData=h.handles.profileData;
    handles.imageInfo=h.handles.imageInfo;
    
    range=inputdlg('How far should we integrate (in pixels)');
    range=str2num(range{1});
    
    
    colors=['r', 'g', 'b', 'c', 'y', 'm'];
    colorNames={'Red', 'Green', 'Blue', 'Cyan', 'Yellow', 'Magenta'};
    mucInd=find(cellfun(@numel, strfind(lower(handles.imageInfo.channelNames),'muc')),1);
    if numel(mucInd)>0 & mucInd~=2
        tmp=colors(mucInd);
        colors(mucInd)=colors(2);
        colors(2)=tmp;
        
        tmp=colorNames{mucInd};
        colorNames{mucInd}=colorNames{2};
        colorNames{2}=tmp;
    end
    
    
    
    f=figure;
    numZSlices=0;
    for z=1:numel(handles.profileData)
        
        if numel(handles.profileData(z).straightenedIm)
            numZSlices=numZSlices+1;
        end
    end
    count=0;
    
    for z=1:numel(handles.profileData)
        if numel(handles.profileData(z).straightenedIm)
            count=count+1;
            labelStr=['Z-slice ', num2str(count), ' of ', num2str(numZSlices)];
            
            
            nrows=size(handles.profileData(z).straightenedIm,1);
            ncols=size(handles.profileData(z).straightenedIm,2);
            
            if abs(handles.imageInfo.pixelSizeX-handles.imageInfo.pixelSizeY)<10^-5
                scale=handles.imageInfo.pixelSizeX;
            else
                warning('Warning: non-isotropic pixel size; no scale info included');
                scale=1;
            end
            hold off
            for i=1:size(handles.profileData(z).straightenedIm,3)
                profNAN=handles.profileData(z).straightenedIm(:,:,i);
                profNAN(profNAN==0)=NaN;
                dd.profilesRaw{i}=profNAN;
                dd.range=min(range,size(profNAN,2));
                dd.density{i}=nanmean(profNAN(:,1:dd.range),2);
                dd.positions=scale*(1:numel(dd.density{1}));
                plot(dd.positions,dd.density{i}/nanmean(dd.density{i}),colors(i));
                hold on
                titleText{i}=[handles.imageInfo.channelNames{i}, ': ', colorNames{i}];
            end
            xlabel('Position along epithelium');
            ylabel('Signal enrichment');
            title(titleText);
            fig2pretty(fullfile(dirname,['mean_relative_density_vs_position',num2str(z),'.eps']));
            hold off
            densityPosData(z)=dd;
            
        end
    end
    set(handles.status, 'String', 'Done');
    guidata(hObject, handles);
    save(fullfile(dirname,'densityPosData.mat'), 'densityPosData');
    
end
