im(:,:,1)=imread('/Volumes/GB1/MichaelSforGabe/150305_antrum_crop_contour150305/straightened_z1_c2.tif');
im(:,:,2)=imread('/Volumes/GB1/MichaelSforGabe/150305_antrum_crop_contour150305/straightened_z1_c3.tif');
%%

%%

for i=1:2
imGray(:,:,i)=mat2gray(im(:,:,i));

bg(:,:,i)=imopen(imGray(:,:,i),strel('disk',10,0));

end
%%
crop=@(x) x(:,:,:);
bgSub=crop(imGray-bg);
bgSub(im==0)=0;
maskDepth=23;
bgSub=mat2gray(bgSub);
bgSub(:,1:maskDepth,2)=0;

%bgSub(:,1:50,:)=0;
thresh=[];
maxDepth=500;
t(1)=1;
t(2)=.2;
minSize(1)=50;
minSize(2)=20;

for i=1:2
   i 
thresh(:,:,i)=im2bw(bgSub(:,1:maxDepth,i),t(i)*graythresh(bgSub(:,50:maxDepth,i)));

thresh(:,:,i)=bwareaopen(thresh(:,:,i),minSize(i));
end
%%
mask=imdilate(im(:,1:maxDepth,2)==0,strel('disk',1));

hPyl=logical(thresh(:,:,2));
regions=regionprops(hPyl,mask,'PixelList','maxIntensity');
for i=1:numel(regions)
    P=regions(i).PixelList;
    min(P(:,1))
    if min(P(:,1))==maskDepth+1 || regions(i).MaxIntensity>0
        hPyl(sub2ind(size(hPyl),P(:,2),P(:,1)))=0;
    end
end

%%
set(gcf,'position',[0,1000,1800,500]);
set(gca,'position',[0,0,1,1]);
i=2;
while 1
showMerge(hPyl(:,:,1)',imadjust(im(:,1:maxDepth,i))',0*thresh(:,:,1)'); axis equal; axis off;
pause(.2);
showMerge(0*thresh(:,:,i)',imadjust(im(:,1:maxDepth,i))',0*thresh(:,:,1)');axis equal; axis off;
pause(0.2);
end
%%showMerge(bwareaopen(thresh(:,:,i),minSize),thresh(:,:,i),imadjust(bgSub(:,1:maxDepth,i))); axis equal

%%
% xs=1:size(thresh,2);
% CoMList=[];
% closestPylori=[];
% hasCell=any(thresh,2);
% hasCell=hasCell(:,1) & hasCell(:,2);
% closestApproach=[];
% lastMitosis=[];
% validRow=[];


% 
% 
% 
% hPylSums=sum(thresh(hasCell,:,2),2);
% mitosisSums=sum(thresh(hasCell,:,1),2);
% 
% hPylCut=mean(hPylSums)-std(hPylSums);
% mitosisCut=mean(mitosisSums)-std(mitosisSums);


%thresh(:,:,2)=bwareaopen(thresh(:,:,2),minSize(2));





closestApproach=computeClosestApproaches(thresh);

threshScrambled=cat(3,thresh(:,:,1),thresh(randperm(size(thresh,1)),:,2));
closestApproachScrambled=computeClosestApproaches(threshScrambled);
%%
% for i=1:size(thresh,1)
%     if any(thresh(i,:,1)) && any(thresh(i,:,2)) && sum(thresh(i,:,1))>15 && sum(thresh(i,:,2))>15
%         validRow(i)=1;
%         for j=1:2
%             f=@(x) gaussSmooth(double(x(:)),100);
%             CoM(j)=sum(double(thresh(i,:,j)).*xs)/sum(double(thresh(i,:,j)));
%             r1(i)=corr(f(thresh(i,:,1)),f(thresh(i,:,2)));
%         end
%         closestPylori=[closestPylori;find(thresh(i,:,2),1)];
%         lastMitosis=[lastMitosis; find(thresh(i,:,1),1,'last')];
%         closestApproach=[closestApproach; findClosestApproach(thresh(i,:,2),thresh(i,:,1))];
%         CoMList=[CoMList; CoM];
%     else
%         
%         r1(i)=0;
%     end
% end

f=@(x) numel(find(abs(x)<25))/numel(x);


closestApproachScrambled=[];
r2=[];
toChoose=find(validRow);
inds1=toChoose;
inds2=randsample(toChoose,numel(inds1));
for i=1:numel(inds1)
    %r2(i)=corr(f(thresh(inds1(i),:,1)),f(thresh(inds2(i),:,2)));
    closestApproachScrambled(i)=findClosestApproach(thresh(inds1(i),:,2),thresh(inds2(i),:,1));
end
    
%%
[r,p]=corr(CoMList(:,1),CoMList(:,2));
%%
bgNaN=bgSub;
bgSub(isnan(im))=NaN;

for i=1:2
    profiles(:,i)=nanmean(bgSub(:,:,i),2);
end

g=@(x) zscore(gaussSmooth(double(x(:)),10)-gaussSmooth(double(x(:)),100));
plot(g(profiles(:,1)),'r');
hold on
plot(g(profiles(:,2)),'g');
hold off
%%
[r,p]=corr(profiles(r:,1),profiles(:,2));



%%
hasCell=zeros(size(hPyl,1));
area=hasCell;
position=hasCell;
for i=1:size(hPyl,1)
    if any(hPyl(i,:))
        hasCell(i)=1;
        area(i)=sum(hPyl(i,:));
        position(i)=mean(find(hPyl(i,:)));
        LGR5(i)=sum(bgSub(i,:,1));
    end
end
hasCell=logical(hasCell);





%%
    while 1
showMerge(imadjust(bgSub(:,1:maxDepth,1))',imadjust(bgSub(:,1:maxDepth,2))'); axis equal; axis off;
pause(.2);
showMerge(0*thresh(:,:,i)',imadjust(bgSub(:,1:maxDepth,2))');axis equal; axis off;
pause(0.2);
end
