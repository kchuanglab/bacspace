function [P,N]=readConfigFile(fname,dim)
fidIN=fopen(fname,'r');
%scan file, looking for tiles placed at origin
tline=fgets(fidIN);
lineNum=1;
P=[];
N={};
while ischar(tline)
    if (tline(1) ~='#' && numel(tline)>12)
        isTile(lineNum)=1;
        origLine=strsplit(tline, ';');
        coords=origLine{end};
        
        N{end+1}=origLine{1};
        coords=strsplit(coords(3:end-2),{'(',',',')',' '});
        P(end+1,:)=cellfun(@str2num,coords);
    else
        isTile(lineNum)=0;
        atOrigin(lineNum)=0;
    end
    lineNum=lineNum+1;
    tline=fgets(fidIN);
end