function bounds=boundingBox(C, padValue, imSize, toRound)
    if nargin==1
        padValue=0;
    end
    minX=min(C(:,1))-padValue;
    minY=min(C(:,2))-padValue;
    maxX=max(C(:,1))+padValue;
    maxY=max(C(:,2))+padValue;
    if nargin==4 && toRound
        minX=round(minX);
        minY=round(minY);
        maxX=round(maxX);
        maxY=round(maxY);
    end
    if nargin>=3
        minX=moveToInBounds(minX, imSize(2));
        maxX=moveToInBounds(maxX, imSize(2));
        minY=moveToInBounds(minY, imSize(1));
        maxY=moveToInBounds(maxY, imSize(1));       
    end
    bounds=[minX,maxX,minY,maxY];
end

function indNew=moveToInBounds(indOld, N)
    indNew=min(N, max(1,indOld));
end

