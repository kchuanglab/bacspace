function [z]=nanzscore(x)
    mu=nanmean(x);
    sigma=sqrt(nanmean((x-mu).^2));
    z=(x-mu)/sigma;