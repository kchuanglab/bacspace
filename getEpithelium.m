function epiData=getEpithelium(hObject,currentContour)
    hold off
    handles=guidata(hObject);
    
    set(handles.status, 'String','Getting epithelial contour');
    pause(.0001);
    if ~isfield(handles, 'currentImage') | handles.reload
        set(handles.status, 'String','Loading image');
        pause(.0001);
        if ~isfield(handles, 'imageInfo')
            if ~isstruct(handles.imageInfo)
                epiData=NaN;
                return
            end
        end
        
        handles.currentImage=getImage(handles.imageInfo, handles.zPos, handles.imageInfo.boundaryChannelNumber);
        guidata(hObject,handles);
        
        
    end
    
    handles=guidata(hObject);
    aspectRatio=size(handles.currentImage,1)/size(handles.currentImage,2);
    handles.displayImage=imresize(handles.currentImage, [aspectRatio*handles.params.displaySize, handles.params.displaySize]);
    handles.displayScale=size(handles.currentImage,1)/size(handles.displayImage,1);
    guidata(hObject,handles);
    if handles.params.preprocessBeforeContours
        handles.reload=1;
        if isinteger(handles.displayImage)
            maxVal=intmax(class(handles.displayImage));
        else
            maxVal=max(handles.currentImage(:));
        end
        
        adjIm=@(newMax,im) imadjust(im,[0; newMax],[0; 1]);
        ax=gca;
        imagesc(handles.displayImage);
        axis equal;
        axis tight;
        redrawFunc=@(newMax) imagescSaturate(ax,adjIm(newMax,handles.displayImage));
        
        newMax=adjustmentSlider2(['Adjust threshold initial threshold to enhance contrast'], 0, 1, redrawFunc,.95);
        axis tight;
        handles.displayImage=adjIm(newMax,handles.displayImage);
        handles.currentImage=adjIm(newMax, handles.currentImage);
%        if handles.params.removeDebrisInitialGuess
 %           handles.currentImage(handles.currentImage==max(handles.currentImage(:)))=0;
  %      end
        
        guidata(hObject,handles);
    end
    
    if handles.params.finalImageScaleFactor~=1
        set(handles.status, 'String', 'Resizing image');
        pause(.0001);
        currImage=imresize(handles.currentImage,1./double(handles.params.finalImageScaleFactor), 'bicubic');
    else
        currImage=handles.currentImage;
    end
    numRows=size(currImage,1);
    numCols=size(currImage,2);
    numRowsOrig=size(handles.currentImage,1);
    numColsOrig=size(handles.currentImage,2);
    
    set(handles.status, 'String', 'Computing image gradient');
    pause(.0001);
    
    grad=smoothGrad(currImage, handles.params);
    hold off
    imagesc(grad, 'Parent',handles.axes1);
    hold on
    axis equal;
    axis tight;
    
    
    axis equal;
    axis tight;
    set(handles.status, 'String','Finding initial contour');
    pause(.0001);
    
    guidata(hObject,handles);
    if nargin<2 || numel(currentContour)==0
        if handles.params.manualInitialContour
            C=getManualContour(hObject);
        else
            
        C=getInitialContour(hObject);
        end
    else
        C=currentContour;
    end
    handles=guidata(hObject);
    pause(.0001);
    %imagesc(currImage, 'Parent',handles.axes1);
    hold on;
    %plotC(C,'b',2);
    hold off;
    axis equal; axis tight;
    set(handles.status, 'String','Relaxing contour');
    pause(.0001);
    
    startP=findClosestEdgePoint(C(1,:), numRowsOrig, numColsOrig);
    endP=findClosestEdgePoint(C(end,:), numRowsOrig, numColsOrig);
    
    C=resampleContour([startP; C; endP]);
    
    %contour in original-scale image;
    
    scaleRatio=nthroot(handles.params.prelimImageScaleFactor/handles.params.finalImageScaleFactor,handles.params.numPrelimIterations-1);
    if isnan(scaleRatio)
        scaleRatio=1;
    end
    
    scale=handles.displayScale;
    scaledIm=handles.displayImage;
    imagesc(scaledIm)
    hold on
    Cscale=rescaleContour(C,size(handles.currentImage), size(scaledIm));
    %scatterC(Cscale);
    edgePoint=findClosestEdgePoint(Cscale(1,:), size(scaledIm,1), size(scaledIm,10));
    axis(boundingBox([Cscale(1,:),edgePoint],mean(size(scaledIm))/5));
    m=msgbox('Now click on edge of epithelium closest edge of image, at the start of the contour');
    uiwait;
    [xStart,yStart]=getPoints(1);
    
    
    edgePoint=findClosestEdgePoint(Cscale(end,:), size(scaledIm,1), size(scaledIm,2));
    axis(boundingBox([Cscale(end,:),edgePoint],mean(size(scaledIm))/5));
    m=msgbox('Now click on edge of epithelium closest edge of image, at the end of the contour');
    uiwait;
    [xEnd,yEnd]=getPoints(1);
    
    fixedPointStart=findClosestEdgePoint(rescaleContour([xStart,yStart],size(scaledIm), size(handles.currentImage)), size(handles.currentImage,1), size(handles.currentImage,2));
    fixedPointEnd=findClosestEdgePoint(rescaleContour([xEnd,yEnd],size(scaledIm), size(handles.currentImage)), size(handles.currentImage,1), size(handles.currentImage,2));
    
    for i=1:handles.params.numPrelimIterations-1
        currScale=handles.params.prelimImageScaleFactor/scaleRatio^(i-1);
        %smoothVal=ceil(handles.params.prelimSmoothing/currScale);
        smoothVal=(handles.params.prelimSmoothing/handles.params.prelimImageScaleFactor);
        
        
        pause(.0001);
        smallImOrig=imresize(handles.currentImage,1./currScale, 'bicubic');
        numRows=size(smallImOrig,1);
        numCols=size(smallImOrig,2);
        if handles.params.prelimSmoothing
            smallIm=imfilter(smallImOrig, fspecial('gaussian',ceil((6*smoothVal)),smoothVal));
        else
            smallIm=smallImOrig;
        end
        
        
        
        smallGrad=smoothGrad(smallIm,handles.params);
        
        smallC=resampleContour(rescaleContour(C,size(handles.currentImage), size(smallIm)));
        
        hold on
        
        scaledFixPointStart=findClosestEdgePoint(rescaleContour(fixedPointStart,size(handles.currentImage), size(smallIm)),size(smallIm,1),size(smallIm,2));
        scaledFixPointEnd=findClosestEdgePoint(rescaleContour(fixedPointEnd,size(handles.currentImage), size(smallIm)),size(smallIm,1), size(smallIm,2));
        %get user-input for beginning and end of each contour
        
        d1=sum((scaledFixPointStart-smallC(1,:)).^2);
        d2=sum((scaledFixPointEnd-smallC(1,:)).^2);
        
        if d2<d1
            smallC=flipud(smallC);
        end
        
        %m=msgbox('Now click on the point on the epithelial boundary closest to the edge');
        %set(handles.status,'String', 'Now click on the point on the epithelial boundary closest to the edge');
        %uiwait(m);
        
        %now 'clip off tails' of image if necessary
        smallC_preclip=smallC;
        dsStart=sum((smallC-repmat(scaledFixPointStart,[size(smallC,1),1])).^2,2);
        [~,ind]=min(dsStart);
        smallC(1:ind,:)=[];
        
        dsEnd=sum((smallC-repmat(scaledFixPointEnd,[size(smallC,1),1])).^2,2);
        [~,ind]=min(dsEnd);
        smallC(ind:end,:)=[];
        
        
        smallC=[scaledFixPointStart; smallC; scaledFixPointEnd];
        
        %probably unecessary, may be good if there is
        %non-integer scaling
        startP=findClosestEdgePoint(smallC(1,:), size(smallIm,1), size(smallIm,2));
        endP=findClosestEdgePoint(smallC(end,:), size(smallIm,1), size(smallIm,2));
        
        
        smallC=resampleContour([startP;smallC;endP]);
        smallC=gaussSmoothCOpen(smallC, handles.params.contourSmoothValue/currScale);
        smallC=resampleContour(smallC);
        
        refinedC=smallC;
        refinedC=resampleContour(moveContourFixedEndpoints(refinedC, smallGrad, handles.params,0,handles.params.orderPointsThreshold));
        
        refinedC=gaussSmoothCOpen(refinedC, handles.params.contourSmoothValue/currScale);
        C=resampleContour(rescaleContour(refinedC, size(smallIm),size(handles.currentImage)));
        hold off;
        imagesc(smallIm); colormap gray;
        axis equal
        hold on
        plotC(smallC,'b');
        plotC(refinedC,'r');
        hold off
        set(handles.status, 'String', ['Current scale factor: ',num2str(currScale)]);
        
        pause(.001);
        
    end
    
    currentContour=resampleContour(rescaleContour(C,size(handles.currentImage), size(currImage)));
    
    currentContour=resampleContour(moveContourFixedEndpoints(currentContour, grad, handles.params,0,handles.params.orderPointsThreshold,0));
    
    
    
    
    
    smoothParam=handles.params.contourSmoothValue/double(handles.params.finalImageScaleFactor);
    axis equal;
    
    axis([0,numCols, 0, numRows])
    
    
    if handles.params.preprocessBeforeContours
        handles.currentImage=getImage(handles.imageInfo, handles.zPos, handles.imageInfo.boundaryChannelNumber);
        currImage=imresize(handles.currentImage,1/double(handles.params.finalImageScaleFactor), 'bicubic');
    end
    fixingImage=imresize(smallImOrig,size(currImage));
    handles.reload=0;
    guidata(hObject, handles);
    imagesc(imadjust(currImage)); axis equal;
    bnds=axis(gca);
    while 1
        
        imagesc(fixingImage); axis equal;
        axis(bnds);
        hold on
        plotC(gaussSmoothCOpen(currentContour,smoothParam),'r',2);
        hold off
        pan on
        
        if strcmpi(buttonDialog('Is contour correct? If not, zoom to the region you would like to fix (Zoom before clicking yes/no).' ,'Correct contour?', {'Yes', 'No'}),'Yes')
            break;
        end
        if handles.params.clickPoints
            set(handles.status,'String', 'Now select a number of points (in order) on the correct edge; press enter when done');
        else
        set(handles.status,'String', 'Now correct the contour by drawing a new line to replace inaccurate region');
        end
        pause(.00001);
        pan off;
        %raw bounds
        bndsRaw=axis(gca);
        %rounded values
        bnds=boundingBox([bndsRaw(1), bndsRaw(3); bndsRaw(2), bndsRaw(4)],0,size(currImage),1);
        axis(bnds);
        
        pointsX=currentContour(:,1);
        pointsY=currentContour(:,2);
        
        pointsInWindow=(pointsX<bnds(2) & pointsX>bnds(1)) & (pointsY<bnds(4) & pointsY>bnds(3));
        %check if pointInWindow is contiguous and contains contour points
        if sum(pointsInWindow) && sum(abs(diff(pointsInWindow)))<=2
            
            startInd=max(find(pointsInWindow,1,'first')-1,1);
            endInd=min(find(pointsInWindow,1,'last')+1,numel(pointsInWindow));
            
            subContour=currentContour(pointsInWindow,:);
            
            
            
            fixedSubContour=fixSubContour(subContour, bnds, currImage,grad,smoothParam,handles,fixingImage);
            hold off;
            imagesc((fixingImage)); axis equal; axis(bnds);
            hold on
            plotC(gaussSmoothCOpen(currentContour,smoothParam),'r',2);
            hold off
            if ~isnan(fixedSubContour)
                hold on;
                plotC(gaussSmoothCOpen(fixedSubContour,smoothParam),'g',2);
                hold off;
                
                
                if strcmp(questdlg('Keep fixed contour(green) or revert to old (red)?', '', 'Keep', 'Revert', 'Keep'),'Keep')
                    currentContour=[currentContour(1:startInd,:);fixedSubContour;currentContour(endInd:end,:)];
                end
            else
                warn('Something went wrong; try again');
            end
        else
            warn('Please zoom in further so that the contour in the window is all in one piece');
        end
    end
    
    
    rescaledContour=resampleContour(rescaleContour(currentContour, size(currImage), size(handles.currentImage)));
    epiData.C2=gaussSmoothCOpen(rescaledContour, handles.params.contourSmoothValue);
    currentContour=gaussSmoothCOpen(currentContour, smoothParam);
    hold off
    imagesc(currImage);
    axis equal
    hold on
    plotC(currentContour,'r');
    axis equal;
    axis tight;
    %replace currentImage with non-adjusted one
    
    guidata(hObject, handles);
    set(handles.status, 'String','Done finding epithelial contour');
    pause(.0001);
    
    
    
end

function [fixedSubContour]=fixSubContour(subContour, bounds, image, gradImage,smoothParam,handles,dispImage )
    numRows=size(image,1);
    numCols=size(image,2);
    %bounds of subimage
    rowRange=bounds(3):bounds(4);
    colRange=bounds(1):bounds(2);
    %forward/backward translations of contour for moving to/from
    %sub-image
    shiftC=@(C,x,y) [C(:,1)-x, C(:,2)-y];
    forwardShift=@(C) shiftC(C,bounds(1)-1,bounds(3)-1);
    backwardShift=@(C) shiftC(C,-bounds(1)+1,-bounds(3)+1);
    
    endPoint=subContour(end,:);
    set(handles.status,'String', 'Correcting subcontour');
    hold off
    imagesc(dispImage(rowRange,colRange));
    axis equal;
    
    hold on
    plotC(gaussSmoothCOpen(forwardShift(subContour),smoothParam),'r',2);
    
    if ~handles.params.clickPoints
        userContour=getMouseCurve(handles.figure1, handles, numRows, numCols);
    else
        x=[];
        y=[];
        count=0;
        
        while(numel(x)<2)
            if count>1
                warn('Please select at least 2 points');
            end
            
            pointsToContourFunc=@(pts) getDrawnContour(pts,forwardShift(subContour),smoothParam);
            
            [x,y]=ginputDraw(pointsToContourFunc);
            count=count+1;
        end
        userContour=([x,y]);
    end
    
    
    Cnew=backwardShift(resampleContour(userContour));
    
    dStart=sum((repmat(Cnew(1,:),[size(subContour,1),1])-subContour).^2,2);
    dEnd=sum((repmat(Cnew(end,:),[size(subContour,1),1])-subContour).^2,2);
    
    [~,indStart]=min(dStart);
    [~,indEnd]=min(dEnd);
    
    if indEnd<indStart
        tmp=indEnd;
        indEnd=indStart;
        indStart=tmp;
        Cnew=flipud(Cnew);
    end
    
    C=resampleContour([subContour(1:indStart,:);Cnew;subContour(indEnd:end,:)]);
    set(handles.status, 'String','Refining contour');
    pause(.0001);
    fixedSubContour=backwardShift((moveContourFixedEndpoints(forwardShift(C), gradImage(rowRange,colRange), handles.params,1,handles.params.orderPointsThreshold,0)));
    fixedSubContour=resampleContour(fixedSubContour);
    
    
    
end




function C=getMouseCurve(hObject, handles,numRows, numCols)
    C=[];
    while size(unique(C,'rows'),1)<5
        if numel(C)
            warn('Invalid curve; too short; try again');
            pause(.001);
        end
        zoom off
        while(1)
            h=guidata(hObject);
            if h.buttonDown
                break
            end
            pause(.001)
        end
        clip=@(C) [C(1,1), C(1,2)];
        getPoint=@()clip(get(gca, 'CurrentPoint'));
        C=[];
        C(end+1,:)=getPoint();
        
        hold on
        while(1)
            h=guidata(hObject);
            if ~h.buttonDown
                break
            end
            new=getPoint();
            
            if (new(1)<1 || new(1)>numCols || new(2)<1 || new(2)> numRows) & size(C,1)>10
                new(1)=min(max(new(1),1),numCols);
                new(2)=min(max(new(2),1),numRows);
                C(end+1,:)=new;
                plot([C(end,1), new(1)], [C(end,2), new(2)],'LineWidth',2);
                
                break
            end
            if ~all(C(end,:)==new)
                plot([C(end,1), new(1)], [C(end,2), new(2)],'LineWidth',2);
                C(end+1,:)=new;
            end
            
            pause(.001);
        end
        hold off
        for i=1:size(C,1)
            if C(i,1)<1
                C(i,1)=1;
            elseif C(i,1)>numCols
                C(i,1)=numCols;
            end
            if C(i,2)<1
                C(i,2)=1;
            elseif C(i,2)>numRows
                C(i,2)=numRows;
            end
        end
    end
end


function C=getManualContour(hObject)
    handles=guidata(hObject);
    imagesc(imadjust(handles.currentImage)); axis equal;
    
    
    
    x=[];
    y=[];
    count=0;
    
    while(numel(x)<2)
        if count>1
            warn('Please select at least 2 points');
        end
        
        pointsToContourFunc=@(pts) getDrawnContour2(pts,handles.params.contourSmoothValue);
        
        [x,y]=ginputDraw(pointsToContourFunc);
        count=count+1;
    end
    C=([x,y]);
    C=resampleContour(gaussSmoothCOpen(resampleContour(C),handles.params.contourSmoothValue));
    
end
function C=getInitialContour(hObject)
    handles=guidata(hObject);
    smallIm=imresize(handles.currentImage, 1/handles.params.prelimImageScaleFactor);
    smoothVal=ceil(handles.params.prelimSmoothing/handles.params.prelimImageScaleFactor);
    hold off
    %smallImContrast=imadjust(smallIm, stretchlim(smallIm, .05));
    %smallImBGSub=smallIm-imopen(smallIm, strel('disk', round(size(smallIm,1)/20),0));
    %smallImAve=imfilter(smallImBGSub, fspecial('average', round(size(smallIm,1)/5)));
    %smallImNorm=smallImBGSub./smallImAve;
    smallImContrast=imadjust(smallIm,stretchlim(smallIm,.1));
    %smallImOpen=imopen(smallImContrast, strel('disk', 5,0));
    smallImSmooth=imfilter(smallIm, fspecial('gaussian', 6*smoothVal, smoothVal));
    clear('smallImBGSub', 'smallImAve', 'smallImNorm', 'smallImContrast');
    imagesc(smallIm); colormap gray; axis equal
    
    
    fillConn=4;
    
    m=msgbox('Please click somewhere in the region you do NOT wish to measure (the epithelium, typically)');
    set(handles.status,'String','Please click somewhere inside region you do not wish to measure');
    uiwait(m);
    [xEpi,yEpi]=getPoints(1);
    xEpi=moveToInBounds(round(xEpi),size(smallIm,2));
    yEpi=moveToInBounds(round(yEpi),size(smallIm,1));
    
    
    
    
    m=msgbox({'Now click somewhere in the region you DO want to measure (the lumen, typically)'});
    uiwait(m);
    set(handles.status, 'String', 'Click in region you want to measure');
    
    [xLumen,yLumen]=getPoints(1);
    xLumen=moveToInBounds(round(xLumen),size(smallIm,2));
    yLumen=moveToInBounds(round(yLumen),size(smallIm,1));
    
    
    if smallImSmooth(yLumen,xLumen)>smallImSmooth(yEpi,xEpi)
        boundaryIm=invertIm(smallImSmooth);
    else
        boundaryIm=smallImSmooth;
    end
        
        threshGuess=graythresh(boundaryIm);

    
    
    
    handles=guidata(hObject);
    handles.axis=gca;
    handles.axes1
    guidata(hObject, handles);
    ax=gca;
    m=msgbox({'Adjust threshold so that the epithelium is in one contiguous piece',...
        'and nothing in the lumen is contiguous with the epithelium'});
    uiwait(m);
    set(handles.status,'String',{'Adjust threshold so that the epithelium is in one contiguous piece',...
        'and nothing in the lumen is contiguous with the epithelium'});
    imagesc(handles.displayImage);
    axis equal;
    contourFunc=@(level) computeContourFromLevel(boundaryIm,[yEpi,xEpi],[yLumen,xLumen],smoothVal,level);
    
    redrawFunc=@(level) drawOverlay(ax,handles.displayImage,contourFunc(level));
    foundContour=0;
    while ~foundContour
        level=adjustmentSlider2(['Adjust threshold for initial guess'], 0.0001, .99999, redrawFunc,threshGuess);
        
        contourInfo=contourFunc(level);
        if numel(contourInfo)
            foundContour=1;
        else
            warn('No contour found...try again');
        end
    end
    
    %ask if contour is correct, if not, draw by hand;
    
    
    
    
    C=contourInfo.C;
    
    xs=C(:,1);
    ys=C(:,2);
    edges= (xs==1) | (ys==1) | (xs==size(smallIm,2)) | (ys==size(smallIm,1));
    
    almostEdges=((xs==2) | (ys==2) | (xs==size(smallIm,2)-1) | (ys==size(smallIm,1)-1)) & ~edges;
    pointsNextToAlmostEdges=(circshift(almostEdges,[1,0]) | circshift(almostEdges,[-1,0]));
    
    
    startingPoint=find(pointsNextToAlmostEdges & edges,1);
    
    shift=-startingPoint+1;
    
    C=circshift(C,[shift,0]);
    edges=circshift(edges,[shift,0]);
    C(edges,:)=[];
    
    
    C=rescaleContour(C,size(smallIm), size(handles.currentImage));
    C=resampleContour(gaussSmoothCOpen(resampleContour(C),handles.params.contourSmoothValue));
end

function contourInfo=computeContourFromLevel(image, epiPoint,lumenPoint,smoothVal,level)
    fillConn=4;
    %All points below level
    filledIm=~im2bw(image,level);
    %the epithelium should be bright, so if we fill the background starting
    %from the epithelium, we should approximately identify it. Subtract
    %filledIm because we only want the pixels that change
    epiIm=logical(imfill(filledIm, epiPoint,fillConn) - filledIm);
    
    lumenIm=(imfill(~epiIm,lumenPoint,fillConn)-epiIm)>0;
    lumenIm=imopen(lumenIm,strel('disk', smoothVal,0));
    bdryCell=bwboundaries(lumenIm);
    contourLengths=[];
    foundInd=[];
    for i=1:numel(bdryCell)
        C=fliplr(bdryCell{i});
        contourLengths(i)=size(C,1);
        if inpoly(fliplr(lumenPoint),C)
            foundInd(end+1)=i;
        end
    end
    if numel(foundInd)==1
        useInd=foundInd;
    elseif numel(contourLengths)
        [~,useInd]=max(contourLengths);
    else
        
    end
        C=fliplr(bdryCell{useInd});
    contourInfo.C=C;
    contourInfo.lumenIm=lumenIm;
end




function drawOverlay(ax, bwIm, contourInfo)
    bnds=axis(ax);
    
    scale=size(bwIm,1)/size(contourInfo.lumenIm,1);
    greenIm=imresize(contourInfo.lumenIm,size(bwIm));
    imagesc(bwIm, 'Parent', ax); colormap gray;
    
    hold(ax,'on');
    axis(ax,'equal');
    alpha=0.5;
    green(:,:,2)=uint8(255*alpha*mat2gray(greenIm));
    green(:,:,1)=uint8(zeros(size(greenIm)));
    green(:,:,3)=uint8(zeros(size(greenIm)));
    
    hGreen=image(green,'Parent', ax);
    set(hGreen, 'AlphaData',green(:,:,2),'Parent', ax);
    if numel(contourInfo)
    C2=rescaleContour(contourInfo.C, size(contourInfo.lumenIm), size(bwIm));
    
    plot(C2(:,1),C2(:,2), 'LineWidth',3,'Parent', ax);
    end
    axis(ax,bnds);
    
    hold(ax,'off');
    
end

function imagescSaturate(ax, im)
    bnds=axis(ax);
    imagesc(im, 'Parent', ax); colormap gray;
    hold(ax, 'on');
    axis(ax, 'equal');
    alpha=1;
    if isinteger(im)
        maxVal=intmax(class(im));
        green(:,:,2)=uint8(255*alpha*(im==maxVal));
        green(:,:,1)=uint8(zeros(size(im)));
        green(:,:,3)=uint8(zeros(size(im)));
        hGreen=image(green,'Parent', ax);
        set(hGreen, 'AlphaData',green(:,:,2),'Parent', ax);
    end
    axis(ax,bnds);
    hold(ax,'off');
    
end

function imOut=invertIm(imIn)
    if isinteger(imIn)
        imOut=intmax(class(imIn))-imIn;
    else
        try
        imOut=max(imIn(:))-imIn;
        catch
            error('invalid image type; cannot invert');
        end
    end
        
end


function [x,y]=getPoints(N)
    x=[];
    y=[];
    while 1
        [x,y]=ginput(N);
        if numel(x)~=N
            warn('Not enough points entered; try again');
        else
            return;
        end
    end
end

%helper function for drawing user input: takes a set of points (e.g. from
%ginput, a contour C (defined in the same axes as pts)
function Cdraw=getDrawnContour(pts,C,smoothParam)
    
    %find endpoints closest to contour
    dStart=sum((repmat(pts(1,:),[size(C,1),1])-C).^2,2);
    dEnd=sum((repmat(pts(end,:),[size(C,1),1])-C).^2,2);
    
    [~,indStart]=min(dStart);
    [~,indEnd]=min(dEnd);
    
    if indEnd<indStart
        tmp=indEnd;
        indEnd=indStart;
        indStart=tmp;
        pts=flipud(pts);
    end
    Cdraw=gaussSmoothCOpen(resampleContour([C(1:indStart,:); pts; C(indEnd:end,:)]),smoothParam);
end

function CDraw=getDrawnContour2(pts,smoothParam)
    CDraw=gaussSmoothCOpen(resampleContour(pts),smoothParam);
end
    
    

