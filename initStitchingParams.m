function params=initStitchingParams()

%Path to Fiji application
params.fijiPath=[];

%hidden
%Directory containing images to stitch; if blank, user will be
%prompted.
params.dirname=[];

%When analyzing a z-stack, we select the slice with the most intensity,
%and also include this number of slices above and below. Smaller will
%save memory.
params.numSlices=int8(7);

%Which channels to use for computing overlaps. Either pick a single
%channel, or 'all'
params.channelRange='all';

%Cross-correlation threshold for computing overlap between images 
%Should be between 0 and 1. Higher means that more overlap is required
%for it to be considered a match. For sequential images, zero may be a
%good starting point. For non-sequential iamges, 0.3 is a good 
%starting point. 
params.threshold=double(0.3);

%When optimizing tile placement, Fiji removes links when the maximum
%displacement is larger than the average displacement by this ratio; a high
%ratio means a tile that is way out of position, suggesting that the link
%is miscalculated
params.maxAveRatio=2.0;

%When optimizing tile placement, Fiji removes links that have greater
%absolute displacement than this amount.
params.absoluteDisplacement=2.0;

%Order of files in directory: 'None', 'Sequential', 'Snake rows', 'Snake
%columns', 'Row by row', 'Column by column', 'From file'
params.orderType='Sequential';
    
%Which direction the files go in the horizontal direction
%(relevant for snake/row-by-row/col-by-col only)
params.orderHorizontal='Right';

%Which direction the files go in the vertical direction
%(relevant for snake/row-by-row/col-by-col only)
params.orderVertical='Down';

%Number of images in horizontal direction (grid/snake only)
params.gridSizeX='2';

%Number of images in vertical direction (grid/snake only)
params.gridSizeY='2';

%starting index of files (grid/snake only)
params.startIndex=1;

%file name format; number of i determines number of digits in file name (grid/snake only)
%don't include file suffix (.e.g. .tif)
params.fileNameFormat='img_{iiii}';

%overlap in percent of images (grid/snake only)
params.imageOverlap=5;


%If true, preprocess images freshly; otherwise, it assumes it has already
%generated the pre-processed images.
params.preprocess=logical(1);

%Apply global histogram equalization when computing overlaps. This
%improves contrast, enhancing features, generally making it easier to
%find the correct overlap. The final stitched image is not
%contrast-adjusted. Saturates 
params.equalizeForComputingOverlap=double(.05);


%Apply global histogram equalization to each image prior to stitching. 
params.equalizeFinalImage=double(0);


%Apply local histogram equalization when computing overlaps. This
%improves contrast, enhancing features, generally making it easier to
%find the correct overlap. The final stitched image is not
%contrast-adjusted. Should be between zero (off) and one; 0.1 may be a
%good starting point. The final stitched image is not
%contrast-adjusted.
params.claheClipLimit=double(0.1);

%Compute overlaps using a smoothed version of the image; this value is
%the standard deviation of the gaussian smoothing applied. Zero means
%off. The final stitched image is not smoothed
params.smoothing=double(0);

%Remove noise using a 'relative noise transform' prior to stitching.
%Zero means off; non-zero values mean width of the filter used; 1 is a
%good starting point. The final stitched image is not denoised
params.relnoise=int8(1);

%Do an intital estimate of overlaps using a down-sampled version of the
%image. This can yield significant improvement in speed when the number
%of images is large. Zero means off; non-zero values mean the factor by
%which the images will be downsampled. The final stitched image is not 
%downsampled
params.downsampleInitial=double(10);

%If this is not equal to 1, the entire workflow will be downsampled by this
%factor
params.downsampleAll=double(1);

%For the final image, this is the factor by which they are downsampled
params.downsampleFinal=double(1);

%Estimate flat-field image by averaging all images, and subtracting from
%each image before stitching; only useful for large numbers of images with
%inhomogeneous illuination. 
params.subtractAverageImage=logical(0);

%When stitching, the default blending option is to use linear blendign
%between overlapping images. Using the maximum intensity is generally
%worse, but can make it easier to identify errors in stitching.
params.useMaxIntensity=0;

%If true, merge planes/channels from final stitched image in to one 
%hyperstack; disable for very large datasets
params.consolidate=logical(1);

%If true, remove temporary/intermediate files
params.cleanup=logical(1);

