function imOut=to16Bit(im)
    imOut=uint16((2^16-1)*mat2gray(im));
end
    