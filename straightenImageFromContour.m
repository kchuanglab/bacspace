function [straightenedIm, straightenedDistanceMap, voronoiRegions]=straightenImageFromContour(C, image, distanceMap, distMin, distMax,kernelWidth, write,statusFunc)
   
    
    if nargin<7
        statusFunc=@(x) x;
    end
    if nargin<6
        write=0;
    end
    statusFunc('Starting to compute voronoi mask'); pause(.0001);
    voronoiRegions=computeVoronoiMask(C,[size(image,1), size(image,2)],statusFunc);
    statusFunc('Voronoi mask computed'); pause(.0001)
    if write
        statusFunc('Starting to write voronoi mask'); pause(.0001);
        imwrite(uint16((voronoiRegions)) , 'voronoi.tif');
    end
    statusFunc('Starting to straighten image'); pause(.0001);
    [straightenedIm, straightenedDistanceMap]=straightenImageFromVoronoi(voronoiRegions, distanceMap, image, distMin, distMax,kernelWidth,statusFunc);
    statusFunc('Image straightened'); pause(.00001);
