function [out]=centralXCorr(im1,im2,sz)
      
    corr=xcorrFFT(im1,im2);
    
    
    centerPoint=(size(corr)+1)/2;
    
    out=corr((centerPoint(1)-sz):(centerPoint(1)+sz), (centerPoint(2)-sz):(centerPoint(2)+sz));
end

    
