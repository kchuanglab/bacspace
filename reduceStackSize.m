function reduceStackSize(dirName, numSlices,scaleFactor, channelNum)
outDir=fullfile(dirName,'reduced');
makeCleanDir(outDir);
imageList=dir(dirName);


if scaleFactor>1
    scaleFactor=1/scaleFactor;
end

    bfFileTypes=load('bfFileExt.mat');

%remove non-image files
toRemove=zeros(size(imageList));
for i=1:numel(imageList)
    [~,~,type]=fileparts(imageList(i).name);
    if imageList(i).isdir
        toRemove(i)=1;
    end
    if imageList(i).name(1)=='.'
        toRemove(i)=1;
    end
    if ~sum(cellfun(@numel,(strfind(bfFileTypes.fileExt(:,1),type))))
        toRemove(i)=1;
    end
    if strfind(type, 'txt')
        toRemove(i)=1;
    end
    
    
end
lowIndList=[];
highIndList=[];
imageList(logical(toRemove))=[];
for i=1:numel(imageList)
    %use first period to split file name so things like '.ome.tif' work
    firstPeriod=find(imageList(i).name=='.',1);
    imageName=imageList(i).name(1:firstPeriod-1);
    disp(['Processing image: ',imageName]);
    imInfo=loadBF('',fullfile(dirName, imageList(i).name),0);
    meanList=zeros(imInfo.nZPlanes,1);
    sampleIm=imresize(getImage(imInfo,1,1),scaleFactor);
  
    imStack=zeros(size(sampleIm,1),size(sampleIm,2),imInfo.nZPlanes,imInfo.nChannels,'like',sampleIm);
    
    for z=1:imInfo.nZPlanes
        for c=1:imInfo.nChannels
            if scaleFactor~=1
            imStack(:,:,z,c)=imresize(getImage(imInfo,z,c),scaleFactor);
            else
                imStack(:,:,z,c)=getImage(imInfo,z,c);
            end
        end
        m=@(x) mean(x(:));
        meanList(z)=m(imStack(:,:,z,channelNum));
    end
    [~,ind]=max(meanList);
    lowInd=ind-numSlices;
    highInd=ind+numSlices;
    if imInfo.nZPlanes<(2*numSlices+1)
        lowInd=1;
        highInd=imInfo.nZPlanes;
    else
        
        if lowInd<1
            lowInd=1;
            highInd=(2*numSlices+1);
        elseif highInd>imInfo.nZPlanes
            highInd=imInfo.nZPlanes;
            lowInd=highInd-2*numSlices;
        end
    end
    imReduced=imStack(:,:,lowInd:highInd,:);
    disp(['Saving image from z=',num2str(lowInd),' to z=',num2str(highInd)]);
    outputFile=fullfile(outDir,[imageName,'.ome.tif']);
    disp(['Saving image to ',outputFile]);
    lowIndList=[lowIndList,lowInd];
    highIndList=[highIndList,highInd];
    bfsave(imReduced,outputFile,'BigTiff',true,'PixelSize', imInfo.pixelSizeX/scaleFactor, 'VoxelThickness', imInfo.pixelSizeZ);
    imInfo.reader.close();
end
save('reduceOutput','lowIndList','highIndList');


end
function makeCleanDir(name)
if isdir(name)
    delete(fullfile(name, '*'));
else
    mkdir(name);
end
end
function [lowInd,highInd]=findZRange(imInfo, numSlices, channelInd)
if imInfo.nZPlanes==1
    lowInd=1;
    highInd=1;
    return;
end
meanList=zeros(imInfo.nZPlanes,1);

for z=1:imInfo.nZPlanes
    im=getImage(imInfo,z, channelInd);
    meanList(z)=mean(im(:));
end

[~,ind]=max(meanList);
lowInd=ind-numSlices;
highInd=ind+numSlices;
if imInfo.nZPlanes<(2*numSlices+1)
    lowInd=1;
    highInd=imInfo.nZPlanes;
else
    
    if lowInd<1
        lowInd=1;
        highInd=(2*numSlices+1);
    elseif highInd>imInfo.nZPlanes
        highInd=imInfo.nZPlanes;
        lowInd=highInd-2*numSlices;
    end
end
end