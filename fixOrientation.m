function [epiDataOut]=fixOrientation(epiData,imSize)
    numRows=imSize(1);
    numCols=imSize(2);
        C_open=epiData.C_open;
        C_closed=epiData.C_closed;
        C_boundary=epiData.C_boundary;
        
        %corner point
        x0=C_boundary(1,1);
        y0=C_boundary(1,2);
        %move inside boundary;
        if x0==0
            x0=1;
        else
            x0=numCols;
        end
        if y0==0
            y0=1;
        else
            y0=numRows;
        end
        
        %adapted from matlab central, implementation of winding angle
        xv = C_closed(:,1);
        yv=C_closed(:,2);
        x1 = xv(1:end-1); y1 = yv(1:end-1);
        x2 = xv(2:end); y2 = yv(2:end);
        a = atan2((x1-x0).*(y2-y0)-(y1-y0).*(x2-x0), ...
            (x1-x0).*(x2-x0)+(y1-y0).*(y2-y0));
        w = sum(a)/(2*pi);
        
        if w<0
            C_open=flipud(C_open);
            C_boundary=flipud(C_boundary);
            C_closed=[C_open; C_boundary; C_open(1,:)];
        end
        
        epiDataOut=epiData;
        epiDataOut.C_open=C_open;
        epiDataOut.C_boundary=C_boundary;
        epiDataOut.C_closed=C_closed;
end
        
    