function Pout=orderPointsGUT(Pin, start,thresh,drawVal)
    if size(Pin,1)<=3
        Pout=Pin;
        return;
    end
    
    if nargin<4
        drawVal=0;
    end
    ds=squareform(pdist(Pin));
    ds(ds>thresh)=0;
    T=graphminspantree(sparse(ds));
    
    U=T;
    U(U~=0)=1;
    U=U+U';
    T=T+T';
 

    [~,path2]=graphshortestpath(T, 1, size(Pin,1));
    
    Pout=Pin(path2',:);
    
    if drawVal
        figure(6)
        hold on;
        scatterC(Pin,'g');
        for i=1:size(T,1)
            for j=1:size(T,2)
                if U(i,j)
                    plotC([Pin(i,:);Pin(j,:)]+[.1,.1;.1,.1],'g');
                end
            end
        end
        plotC(Pout,'r');
        hold off;
        title('order points');
    end
end
    
    
    
