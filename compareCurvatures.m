%function compareCurvatures

data(1)=load('/Volumes/homes/Group/MicriobiomeImaging/BtMonoCrossSections/BtSTD20umcrosssection20x7x8_split/processed/cropped2_contour150213.mat');
data(2)=load('/Volumes/homes/Group/MicriobiomeImaging/BtMonoCrossSections/BtPD3crosssection20x6x7_split/processed/croppedRotate_contour150210.mat');

bdry(1)=load('/Volumes/homes/Group/MicriobiomeImaging/BtMonoCrossSections/BtSTD20umcrosssection20x7x8_split/processed/150213_cropped2_contour150213/boundaryData.mat');
bdry(2)=load('/Volumes/homes/Group/MicriobiomeImaging/BtMonoCrossSections/BtPD3crosssection20x6x7_split/processed/150210_croppedRotate_contour150210/boundaryData.mat');
%%
smMicron=20;
for i=1:2
    scale(i)=data(i).handles.imageInfo.pixelSizeX*10^4;
    C{i}=resampleContour(data(i).handles.epiData.C_open*scale(i));
    Csm{i}=resampleContour(gaussSmoothCOpen(C{i},smMicron));
    curv{i}=-contourCurvature(Csm{i});
    R{i}=1./curv{i};
    m=bdry(i).boundaryData.edgeLocs{1}*10^4;
    mucThickness{i}=m(1)+resample(m'-m(1),numel(curv{i}),numel(m));
    [rval(i),pval(i)]=corr(mucThickness{i},curv{i});
   [k,m,e]=running_average(curv{i},mucThickness{i},.2*10^-2);
   errorbar(k,m,e);
   xlabel('Curvature (\mum^{-1})');
   ylabel('Mucus thickness (\mum)');
   hold on
   axis([-.02,.02,0,80]);
   
end

%%
    nhist(curv,'samebins','legend',{'STD','PD'})
    xlabel('Curvature (\mum^{-1})');
    
    
