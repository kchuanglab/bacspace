function [ns]=normals(ts)
    if size(ts,2)~=2
        error('input vector must be list of 2-d points with each point in a row');
    end
    
    tlengths=sqrt(sum(ts.^2,2));
   
    ns=[ts(:,2),-1*ts(:,1)]./repmat(tlengths, [1,2]);
