function Xout=interpolateProfile2(V,interpolatedPoints,method,extrapval,maxRange)
    
    if nargin<3
        method='linear';
    end
    if nargin<4
        extrapval=NaN;
    end
    if nargin<5
        maxRange=10;
    end
        interpolatedPoints=imdilate(interpolatedPoints,strel('disk',20));

    nanDist=bwdist(interpolatedPoints);
    
    nanVals=interpolatedPoints;
    nanCoords=find(nanVals);
    [xMesh,yMesh]=meshgrid(1:size(V,2), 1:size(V,1));
    Xout=double(V);
    
    cut=nanDist>0 & nanDist<maxRange;
    
    Xcut=xMesh(cut);
    Ycut=yMesh(cut);
    Vcut=double(V(cut));
    
    F=scatteredInterpolant(Xcut,Ycut,Vcut,method);
    
    Xout(nanCoords)=F(xMesh(nanCoords), yMesh(nanCoords));
    
    Xout(Xout>max(V(:)))=max(V(:));
    Xout(Xout<min(V(:)))=min(V(:));