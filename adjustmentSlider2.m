function [finalValue]=adjustmentSlider2(textVal, minVal, maxVal, redrawFunc, defaultVal)
    if nargin<5
        defaultVal=mean([minVal,maxVal]);
    end
    parentHandle=gcbo;
    figHandle=figure('Name', textVal);
    width=400;
    height=200;
    numRows=1;
    numCols=1;
    
    set(figHandle, 'Position', [400,400,numCols*width,numRows*height], 'Menubar', 'none','Toolbar', 'none');
    set(figHandle, 'CloseRequestFcn', @closeFun);
    
    
    pos=get(figHandle, 'Position');
    slider=uicontrol(figHandle,'Style', 'slider', 'Position',[0,0,width,height/4],...
        'Min',minVal, 'Max', maxVal, 'Value', defaultVal, ...
        'Callback', @sliderCallback,...
        'SliderStep', [.001,.001]);
    textBox=uicontrol(figHandle,'Style', 'text', 'String',...
        {'Select threshold'} , ...
        'Position',[0,height/2,width,height/2],...
        'FontSize', 14);

    button=uicontrol(figHandle, 'Style','pushbutton', 'Position', [width/4,height/2, width/2,height/4], 'String', 'Okay', 'CallBack', @closeFun);
    childData=guidata(figHandle);
    childData.parentHandle=parentHandle;
    childData.slider=slider;
    childData.redrawFunc=redrawFunc;
    guidata(figHandle,childData);
    redrawFunc(defaultVal);
    uiwait(figHandle);
    
    handles=guidata(parentHandle);
    finalValue=handles.val;
    rmfield(handles,'val');
    guidata(parentHandle, handles);
    
function []=closeFun(src,event)
    childData=guidata(src);
    parentData=guidata(childData.parentHandle);
    parentData.tmp=[];
    parentData.val=(get(childData.slider,'Value'));
    %parentData.params
    guidata(childData.parentHandle,parentData);
    delete(gcf);
    
function sliderCallback(hObject,event,comment)
    childData=guidata(hObject);
    val=(get(gcbo, 'Value'));
    childData.redrawFunc(val);