function Cout=rescaleContour(C,szOld, szNew)
    Cout=[rescaleVector(C(:,1),szOld(2),szNew(2)),rescaleVector(C(:,2), szOld(1), szNew(1))];
end

function xOut=rescaleVector(x,oldMax,newMax)
    xOut=(x-1)*((newMax-1)/(oldMax-1))+1;
end