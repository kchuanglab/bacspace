function [comments]=importComments(fname)
    
    params=eval(fname(1:end-2));
    paramNames=fieldnames(params);
    linesList={};
    fid=fopen(fname,'r');
    tline = fgets(fid);
    while ischar(tline)
        linesList{end+1}=tline;
        tline = fgets(fid);
    end
    
    fclose(fid);
    comments=cell(numel(paramNames),1);
    for i=1:numel(paramNames)
        name=paramNames{i};
        matches=cellfun(@numel,strfind(linesList,['params.',name,'=']));
        ind=find(matches);
      
        if numel(ind)==0
            error('invalid parameter parsing');
        end
        if numel(ind)>1
            error('multiple matches');
        end
        
        comments{i}=[];
        lineNum=ind-1;
        while lineNum>=1 && linesList{lineNum}(1)=='%'
            comments{i}=[linesList{lineNum}(2:end),comments{i}];
            lineNum=lineNum-1;
        end
        
    end
    
    