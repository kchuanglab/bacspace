function indNew=moveToInBounds(indOld, N)
    indNew=min(N, max(1,indOld));
end

