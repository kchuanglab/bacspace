function [merge]=showMerge(im1,im2,im3,disp)
    if nargin<3
        im3=zeros(size(im1));
    end
    if nargin<2
        im2=zeros(size(im1));
    end
    if nargin<4
        disp=1;
    end
    
    
    merge(:,:,1)=mat2gray(im1);
    merge(:,:,2)=mat2gray(im2);
    merge(:,:,3)=mat2gray(im3);
    if disp
        imagesc(merge);
    end