function voronoiRegions=computeVoronoiMask(C, imSize,statusFunc)
    if nargin<3
        statusFunc=@(x) x;
    end
    maxD=1000*max(C(:));
    C_bdry=[maxD,maxD; -maxD,maxD; maxD, -maxD; -maxD,-maxD; 0,maxD; 0, -maxD; maxD,0; -maxD, 0];
    
    [V,R]=voronoin([C;C_bdry]);
    R=cellfun(@uint16, R,'UniformOutput', false);
    
    statusFunc('Computed Voronoi diagram'); pause(.0001);
    [xmesh,ymesh]=meshgrid(1:imSize(2), 1:imSize(1));
    voronoiRegions=cast(zeros(imSize),'uint16');
    statusFunc('Computing voronoi mask'); pause(.0001);
    for cellInd=1:numel(R)
        
        if (numel(R{cellInd})>0 & all(R{cellInd}~=1)  )
            
            
            cellVertices=V(R{cellInd},:);
            inPolygonMex=InPolygon(xmesh,ymesh,cellVertices(:,1), cellVertices(:,2));
            
            contourPointsInPolygon=InPolygon(C(:,1),C(:,2),cellVertices(:,1), cellVertices(:,2));
            if numel(find(contourPointsInPolygon))>1
                error('multiple contour points in voronoi region');
            end
            if numel(find(contourPointsInPolygon))==1
                voronoiRegions(inPolygonMex)=uint16(find(contourPointsInPolygon));
            end
            
        end
    end