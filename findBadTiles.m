[P1,N1]=readConfigFile('/Volumes/GB1/WholePellet/BtPD20xMuc2DAPIhalfpellet_split/tmpContrast/config.txt');
[P2,N2]=readConfigFile('/Volumes/GB1/WholePellet/BtPD20xMuc2DAPIhalfpellet_split/tmpContrast/config.registered.txt');

P1=P1-repmat(P1(4,:),[size(P1,1),1]);
P2=P2-repmat(P2(4,:),[size(P2,1),1]);
disp=P2-P1;

d=sqrt(sum(disp.^2,2));
N=N';
N(d>1000,:)c