function [dsOther,dsPair]=distancesFromLabeledImage(labeledImage,maxD)
    if nargin<2
        maxD=Inf;
    end
    % dsPairs: distance of i-pixels to nearest j-pixel
    for i=1:max(labeledImage(:))
        dMap=bwdist(labeledImage ~=i & labeledImage);
        dsOther{i}=dMap(labeledImage==i & dMap<maxD);
        for j=1:max(labeledImage(:))
            dMap=bwdist(labeledImage==j);
            dsPair{i,j}=dMap(labeledImage==i & dMap<maxD);
        end
    end
    
    
    
    