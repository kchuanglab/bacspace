function [epi_mask]=computeMask(C_closed, numX, numY, scaleFactor)
        [xgridScale,ygridScale]=meshgrid(1:scaleFactor:numX, 1:scaleFactor:numY);
        [xgrid,ygrid]=meshgrid(1:numX, 1:numY);
        sizeGrid=size(xgridScale);
        xgridScale=xgridScale(:);
        ygridScale=ygridScale(:);
        
        epi_mask=inpoly([xgridScale,ygridScale], C_closed);
        epi_mask=reshape(epi_mask,sizeGrid);
        epi_mask=imresize(epi_mask, size(xgrid));
end