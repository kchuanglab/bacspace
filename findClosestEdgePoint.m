function Pedge=findClosestEdgePoint(P, numRows, numCols)
    px=min(P(1), numCols+1-P(1));
    py=min(P(2), numRows+1-P(2));
    
    if px<py
        if px==min(P(1))
        Pedge=[1,P(2)];
        else
            Pedge=[numCols,P(2)];
        end
    else
        if py==min(P(2));
            Pedge=[P(1),1];
        else
            Pedge=[P(1),numRows];
        end
    end
end