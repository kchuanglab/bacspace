function [c,n startP, endP]=getSpines(contour,range, closed)
    if nargin<3
        closed=1;
    end
c=centers(contour,closed);
n=normals(tangents(contour,closed));


startP=c+range*n;
endP=c-range*n;


