function [closestApproach]=computeClosestApproaches(thresh)
closestApproach=[];
for i=1:size(thresh,1)
    if any(thresh(i,:,1)) && any(thresh(i,:,2)) && sum(thresh(i,:,1))>15 && sum(thresh(i,:,2))>15
       
        closestApproach=[closestApproach; findClosestApproach(thresh(i,:,2),thresh(i,:,1))];
    else
        
    end
end
