function [im]=getImage(imageInfo, z,c)
    dimOrder=imageInfo.reader.getMetadataStore().getPixelsDimensionOrder(0).toString().toCharArray()';
    
    if strcmpi(dimOrder,'XYZCT')
        im=bfGetPlane(imageInfo.reader, imageInfo.nZPlanes*(c-1)+z);
    else
        if ~strcmpi(dimOrder, 'XYCZT')
            
            warning('Unknown dimension order: ',dimOrder,'; assuming XYCZT');
            
        end
        im=bfGetPlane(imageInfo.reader,imageInfo.nChannels*(z-1)+c);
    end