function Cout=moveContourFixedEndpoints(Cin, image, params,fixLast,orderPointsDist,debug)
    if nargin<4
        fixLast=1;
    end
    if nargin<5
        orderPointsDist=params.range+1;
    end
    if nargin<6
        debug=0;
    end
    
    Cin(:,1)=moveToInBounds(Cin(:,1),size(image,2));
    Cin(:,2)=moveToInBounds(Cin(:,2),size(image,1));
    %start/end points of the spines we're measuring
    [~,~, startP, endP]=getSpines(Cin, params.range,0);
    % create mesh of points along each spine
    frac = 0:1./(params.nPointsToSample-1):1;
    %(i,j) corresponds to the coordinates of the jth point in the ith spine
    XX = repmat(startP(:,1),1,params.nPointsToSample)+repmat(frac,size(startP,1),1).*repmat(endP(:,1)-startP(:,1),1,params.nPointsToSample);
    YY = repmat(startP(:,2),1,params.nPointsToSample)+repmat(frac,size(startP,1),1).*repmat(endP(:,2)-startP(:,2),1,params.nPointsToSample);
    
    %image coordinates
    [X,Y] = meshgrid(1:size(image,2),1:size(image,1));
    % interpolate
    interp_fluo = interp2(X,Y,double(image),XX,YY,'bilinear');
    Cout=zeros(size(XX,1)+1+fixLast,2);
    Cout(1,:)=Cin(1,:);
    
    for i=1:size(XX,1)
        
        [maxVal,ind]=max(interp_fluo(i,:));
        
        
        [~,peakLocs]=getPeaksAboveMinPeakHeight(interp_fluo(i,:) ,params.peakThreshold*maxVal);
        
        if debug
            figure(1)
            subplot(121)
            imagesc(image);
            axis(boundingBox(Cin(i,:),50));
            axis off;
            colormap gray;
            hold on
            scatter(XX(i,:),YY(i,:),'b');
            plotC(Cin);
            plotC(Cout(1:i,:),'r');
            hold off
            subplot(122)
            
            plot(interp_fluo(i,:));
        end
        if numel(peakLocs)==0
            
            Cout(i+1,:)=[XX(i,ind),YY(i,ind)];
            
        else
            
            peakPoints=[XX(i,peakLocs)', YY(i,peakLocs)'];
            
            %add in parameter if desired
            if 1
                prediction=Cout(i,:);
            else
                prevPoints=Cout(max(i-params.fittingStraightness+1,1):i,:);
                [m,b]=fitToLine(prevPoints(:,1), prevPoints(:,2));
                
                %average step size in x;
                dx=mean(diff(prevPoints(:,1)));
                prediction=[prevPoints(end,1)+dx,prevPoints(end,2)+m*dx];
            end
            
            
            
            ds2=sum((peakPoints-repmat(prediction,[numel(peakLocs),1])).^2,2);
            
            [~,ind]=min(ds2);
            Cout(i+1,:)=peakPoints(ind,:);
            if debug
                subplot(121)
                hold on
                scatterC(peakPoints,'r');
                scatterC(Cout(i+1,:),'g');
                hold off
            end
            
        end
        if debug
            pause(.01);
        end
        
        
    end
    if fixLast
        Cout(end,:)=Cin(end,:);
    end
    
    %new point is maximum gradient
    %[~,inds]=max(interp_fluo,[],2);
    %indsLinear=sub2ind(size(XX), (1:size(XX,1))', inds);
    
    
    
    
    
  
    Cout=orderPointsGUT(Cout, 1,orderPointsDist,0);
    if numel(Cout)<10
        error('Empty contour after moveContourFixedEndpoints');
    end
end


function [slope,intercept]=fitToLine(x,y)
    n=numel(x);
    mx=mean(x);
    my=mean(y);
    xy=sum(x.*y);
    x2=sum(x.^2);
    slope=(xy-n*mx*my)/(x2-n*(mx^2));
    intercept=(my*x2-mx*xy)/(x2-n*(mx^2));
end
    
    
    
    

